var tinymce_upload = {
    modal_id : "#tinymce-media-dialog",
    init : function(){
        var self = this;
        
        $('#tinymce_form_upload input[name="tinymce_image"]').bind("change", function(){
            $('#tinymce_form_upload').trigger("submit");
            $(this).val("");
        });
                
        $("form#tinymce_form_upload").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.upload_process(formData);
        });
        
        $('#tinymce-media-dialog-tab a').live('click', function(e){
            e.preventDefault();
            $(this).tab('show');
        });
        
        
        //CHECK UNCHECK ALL IMAGES
        $("#check-uncheck-all-tinymce-media-img").live('click', function(e){
            e.preventDefault();
            if($(this).attr('is_checked').match(/^no$/i)){
                $("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]").attr("checked", true);
                $(this).attr('is_checked', 'yes');
                $(this).find('a').html('uncheck all');
            }else{
                $("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]").attr("checked", false);
                $(this).attr('is_checked', 'no');
                $(this).find('a').html('<i class="icon-check"></i>&nbsp;check all');
            }
        });
        
        //DELETE SELECTED IMAGES
        $("#delete-selected-tinymce-media-img").live('click', function(e){
            e.preventDefault();
            if($("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]:checkbox:checked").length == 0){
                alert("no image selected");
                return false;
            }else{
                //delete image selected
                self.delete_process();
            }
        });
        
        //INSERT SELECTED IMAGES
        $("#insert-selected-tinymce-media-img").live('click', function(e){
            e.preventDefault();
            if($("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]:checkbox:checked").length == 0){
                alert("no image selected");
                return false;
            }else{
                //insert image selected
                $("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]:checked").each(function() {
                    tinymce.activeEditor.execCommand('insertHTML', false, '<img src="'+$(this).attr('img_url')+'" alt="'+$(this).attr('filename')+'">');
                });
            }
        });
        
        self.read_uploads_dir();        
        
    },
    
    read_uploads_dir : function(){
        $.ajax({
            url : BASE_URL+"tinymce_media_dialog/read_uploads_dir",
            type : 'post',
            dataType : 'json',
            async : true,
            error : function(request){
                console.log(request.responseText);
            },
            success : function(json){
                $(tinymce_upload.modal_id).find(".modal-body").html(json.map_view);
            }
        }); 
    },
    
    upload_process : function(formData){
        $.ajax({
            url : BASE_URL+"tinymce_media_dialog/tinymce_upload_process",
            type : 'post',
            data : formData,
            dataType : 'json',
            async : true,
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            beforeSend : function(){
                $(".loading_tinymce_upload").show();
            },
            error : function(request){
                $(".loading_tinymce_upload").hide();
                console.log(request.responseText);
            },
            success : function(json){
                $(".loading_tinymce_upload").hide();
                if(json.status.match(/^success$/i)){
                    //top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('%s').closest('.mce-window').find('.mce-primary').click();
                    //tinymce.activeEditor.execCommand('insertHTML', false, '<img src="'+json.upload_data.url+'">');
                    $("#tinymce-media-img-ul").prepend(json.li_img);
                }else{
                    alert(json.msg);
                }
            }
        }); 
    },
    
    
    delete_process : function(){
        var img = [];
        $("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]:checked").each(function() {
            img.push($(this).attr("filename"));
        });
        
        $.ajax({
            url : BASE_URL+"tinymce_media_dialog/tinymce_delete_process",
            type : 'post',
            data : {
                'img' : img
            },
            dataType : 'json',
            async : true,
            beforeSend : function(){
                $(".loading_tinymce_upload").show();
            },
            error : function(request){
                $(".loading_tinymce_upload").hide();
                console.log(request.responseText);
            },
            success : function(json){
                $(".loading_tinymce_upload").hide();
                if(json.status.match(/^success$/i)){
                    $("input[type=checkbox][name=tinymce-media-dialog-checkbox-img]:checked").each(function() {
                        $(this).parents("li").remove();
                    });
                }else{
                    alert(json.msg);
                }
            }
        }); 
                        
    }
    
};

$(function(){
    myTinyMCE.options.file_browser_callback = function(field_name, url, type, win) {
        if(type=='image') $(tinymce_upload.modal_id).modal("show");
    };
    tinymce_upload.init();
//$(tinymce_upload.modal_id).modal("show");
});



