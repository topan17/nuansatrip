var site_backend = {
    init: function() {
        var self = this;
        self.init_sidebar();
    },
    init_sidebar: function() {
        var menu_active = '';
        $('a.list-group-item').each(function() {
            if ($(this).attr('data-src') == class_name) {
                menu_active = $(this);
                menu_active.parents().addClass('in');
                menu_active.addClass('active');
            }
        });
    },
    show_loading_animation: function(options) {
        var default_options = {
            css_background: {
                'width': '100%',
                'height': '100%',
                'background-color': '#000',
                'opacity': '0.5',
                'z-index': '9998',
                'position': 'fixed'
            },
            css_box_img: {
                'position': 'fixed',
                'top': '50%',
                'left': '50%',
                'margin-left': '-35px',
                'margin-top': '-35px',
                'background-color': '#fff',
                'z-index': '9999',
                'border-radius': '50%'
            },
            img: '<div id="box-img"><img src="' + _assets + 'img/loading.gif" width=70></div>',
            text_loading: 'Please wait'
        };

        options = (typeof (options) !== "undefined") ? $.extend(default_options, options) : default_options;

        //Generate loading HTML
        var loading_html = '<div id="bg-loading-animation"></div>' + options.img;

        $('body').prepend(loading_html);
        $('body').css('overflow', 'hidden');
        $('body').find('#bg-loading-animation').css(options.css_background);
        $('body').find('#box-img').css(options.css_box_img);
    },
    hide_loading_animation: function() {
        $('body').css('overflow', 'auto');
        $('body').find('#bg-loading-animation').remove();
        $('body').find('#box-img').remove();
    },
    clean_form: function(formName) {
        $('form#' + formName + ' div.error').remove();
        $('form#' + formName + ' div.success').remove();
        $('form#' + formName + ' input').removeClass('error');
        $('form#' + formName + ' textarea').removeClass('error');
    },
    clean_form_reset: function(formName) {
        $('form#' + formName)[0].reset();
    }
};

$('document').ready(function() {
    site_backend.init();
});
