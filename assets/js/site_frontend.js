var site_frontend = {
    init: function() {
        var self = this;

        self.set_login_popup_event();
        self.set_register_popup_event();
        self.set_request_activasion_popup_event();
        self.set_request_forgotpass_popup_event();

        $(window).scroll(function() {
            if ($(this).scrollTop() > 110) { //use `this`, not `document`
                if($('.main-header ul.menu li.dropdown').hasClass('open')){
                    $('.main-header ul.menu li.dropdown').removeClass('open');
                }

                $(".chaser").slideDown(400, function() {
                    $('.chaser').css({
                        'display' : 'block'
                    });
                });

            } else {
                if($('.chaser ul.menu li.dropdown').hasClass('open')){
                    $('.chaser ul.menu li.dropdown').removeClass('open');
                }

                $(".chaser").slideUp(200, function() {
                    $('.chaser').css({
                        'display' : 'none'
                    });
                });
            }
        });

        $("body").delegate('a#back-to-top', 'click', function(e) {
            e.preventDefault();
            $("html, body").animate({
                scrollTop: 0
            });
        });

        $("body").delegate('a.goto-signup', 'click', function(e) {
            e.preventDefault();
            $(".modal").modal('hide');
            $("#popup_box_signup").modal('show');
            self.clean_form("form-popup-register");
            self.clean_form_reset("form-popup-register");
        });

        $("body").delegate('a.goto-login', 'click', function(e) {
            e.preventDefault();
            $(".modal").modal('hide');
            $("#popup_box_login").modal('show');
            self.clean_form("form-popup-login");
            self.clean_form_reset("form-popup-login");
        });

        $("body").delegate('a.goto-forgotpass', 'click', function(e) {
            e.preventDefault();
            $(".modal").modal('hide');
            $("#popup_box_forgotpass").modal('show');
            self.clean_form("form-popup-forgotpass");
            self.clean_form_reset("form-popup-forgotpass");
        });

        $("body").delegate('a.goto-request-activasion', 'click', function(e) {
            e.preventDefault();
            $(".modal").modal('hide');
            $("#popup_box_activasion").modal('show');
            self.clean_form("form-popup-activasion");
            self.clean_form_reset("form-popup-activasion");
        });
    },

    showRecaptcha: function(element) {
        Recaptcha.create("6LeH1_sSAAAAADMbkEfqYTMr865otBMRgMzdx556", element, {
            theme: "custom"
        //callback: Recaptcha.focus_response_field
        });
    },

    set_login_popup_event: function() {
        var self = this;
        $("form#form-popup-login").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.login_member(formData, 'form-popup-login');
        });
    },

    set_register_popup_event: function() {
        var self = this;
        $("form#form-popup-register").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.register_member(formData, 'form-popup-register');
        });
    },

    set_request_activasion_popup_event: function() {
        var self = this;
        $("form#form-popup-activasion").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.request_activasion(formData, 'form-popup-activasion');
        });
    },

    set_request_forgotpass_popup_event: function() {
        var self = this;
        $("form#form-popup-forgotpass").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.request_forgotpass(formData, 'form-popup-forgotpass');
        });
    },

    login_member: function(formData, formName) {
        var self = this;
        var query_string = typeof(GET) !== "undefined" ? "?" +$.param(GET) : "";
        $.ajax({
            url: base_url + "login/login_member" + query_string,
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                if(json.status === 'success') {
                    $("#" + formName).append(json.form_success);
                    setTimeout(function() {
                        window.location.href = json.urlgoto != '' ? json.urlgoto : current_url;
                    }, 500);
                } else {
                    self.hide_loading_animation();
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field +"]");
                        var form_group = input_form.parents(".form-group");

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }
                    if(json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
            }
        });
    },

    register_member: function(formData, formName) {
        var self = this;
        var query_string = typeof(GET) !== "undefined" ? "?" +$.param(GET) : "";
        $.ajax({
            url: base_url + "register/register_member" + query_string,
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                if(json.status === 'success') {
                    $(".modal").modal('hide');
                    self.clean_form_reset(formName);
                    $("#popup_box_alertactivasion span.member_email_activasion").html(json.member_email);
                    $("#popup_box_alertactivasion").modal('show');
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field +"]");
                        var form_group = input_form.parents(".form-group");

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }

                    if(json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
                self.hide_loading_animation();
            }
        });
    },

    request_activasion: function(formData, formName) {
        var self = this;
        var query_string = typeof(GET) !== "undefined" ? "?" +$.param(GET) : "";
        $.ajax({
            url: base_url + "activasion/request_activasion" + query_string,
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again.</div>');
            },
            success: function(json) {
                if(json.status === 'success') {
                    $("#" + formName).append(json.form_success);
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field +"]");
                        var form_group = input_form.parents(".form-group");

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }

                    if(json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
                self.hide_loading_animation();
            }
        });
    },

    request_forgotpass: function(formData, formName) {
        var self = this;
        $.ajax({
            url: base_url + "forgot_password/request_forgotpass",
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again.</div>');
            },
            success: function(json) {
                if(json.status === 'success') {
                    $("#" + formName).append(json.form_success);
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field +"]");
                        var form_group = input_form.parents(".form-group");

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }

                    if(json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
                self.hide_loading_animation();
            }
        });
    },
    
    show_loading_animation: function(options) {
        var default_options = {
            css_background: {
                'width': '100%',
                'height': '100%',
                'background-color': '#000',
                'opacity': '0.5',
                'z-index': '9998',
                'position': 'fixed'
            },
            css_box_img: {
                'position': 'fixed',
                'top': '50%',
                'left': '50%',
                'margin-left': '-35px',
                'margin-top' : '-35px',
                'background-color': '#fff',
                'z-index': '9999',
                'border-radius': '50%'
            },
            img: '<div id="box-img"><img src="' + _assets + 'img/loading.gif" width=70></div>',
            text_loading: 'Please wait'
        };

        options = (typeof (options) !== "undefined") ? $.extend(default_options, options) : default_options;

        //Generate loading HTML
        var loading_html = '<div id="bg-loading-animation"></div>' + options.img;

        $('body').prepend(loading_html);
        $('body').css('overflow','hidden');
        $('body').find('#bg-loading-animation').css(options.css_background);
        $('body').find('#box-img').css(options.css_box_img);
    },

    hide_loading_animation: function() {
        $('body').css('overflow','auto');
        $('body').find('#bg-loading-animation').remove();
        $('body').find('#box-img').remove();
    },
    clean_form: function(formName) {
        $('form#' + formName + ' div.error').remove();
        $('form#' + formName + ' div.success').remove();
        $('form#' + formName + ' input').removeClass('error');
        $('form#' + formName + ' textarea').removeClass('error');
    },
    clean_form_reset: function(formName) {
        $('form#' + formName)[0].reset();
    }
};

$('document').ready(function() {
    site_frontend.init();
});
