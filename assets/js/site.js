var site = {
    init: function() {
        var self = this;

        $("body").delegate(".prevent-default", "click", function(e) {
            e.preventDefault();
        });


        /**
         * @desc event handler, change dropdown language
         */
        $("[name=change-language]").bind('click', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                type: 'post',
                dataType: 'json',
                async: true,
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    if (json.status.match(/^success$/i)) {
                        location.reload();
                    }
                }
            });
        });

        /**
         * @desc event handler, change dropdown currency
         */
        $("[name=change-currency]").bind('click', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                type: 'post',
                dataType: 'json',
                async: true,
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    if (json.status.match(/^success$/i)) {
                        location.reload();
                    }
                }
            });
        });


        $("a[name=log-out-btn]").bind('click', function(e) {
            e.preventDefault();
            window.location = $(this).attr('href') + "?urlgoto=" + current_url;
        });

        self.init_datepicker();


        //Event handler when delete button in toogle-cart is clicked
        $("body").delegate('.del-goods', 'click', function(e) {
            var remove_btn = $(this);
            e.preventDefault();
            $.ajax({
                url: class_url + "delete_cart_item",
                data: {
                    package_id: $(this).attr('package_id'),
                    member_store_id: $(this).attr('store_id')
                },
                type: 'post',
                dataType: 'json',
                async: true,
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    if (json.status.match(/^success$/i)) {
                        remove_btn.parents("li").remove();
                        if ($("ul#cart-items li").length > 0) {
                            $("#cart-info-count").html(($("ul#cart-items li").length) + " items");
                            $("#cart-info-value").html(self.formatNumber(json.total_amount));
                            self.update_scroll_style();
                        } else {
                            self.refresh_toogle_cart_item();
                        }

                    }
                }
            });
        });

        //self.update_scroll_style();
        self.cart_info_update();
        self.set_filter_event_handler(); //Set event-handler for search function
        self.set_search_event_handler(); //Set event-handler for search function
        self.set_modal_confirm_msg_event_handler(); //set modal confirm msg event

    },
    set_theme_builder_events: function() {
        $(".theme_builder").each(function() {
            $(this).off('click');
        });

        $(".theme_builder").bind("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
    },
    init_datepicker: function() {
        //Set Datepicker
        $(document).on('DOMNodeInserted', '.datepicker', function() {
            $(".datepicker").datepicker({
                "dateFormat": "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                yearRange: "-75:+0"
            });
        });
    },
//    update_scroll_style: function() {
//        /**
//         * @desc event scroll handler
//         */
//        $('.scroller').each(function() {
//            var height;
//            if ($(this).attr("data-height")) {
//                height = $(this).attr("data-height");
//            } else {
//                height = $(this).css('height');
//            }
//            $(this).slimScroll({
//                height: height,
//                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
//                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
//                disableFadeOut: true
//            });
//        });
//    },
    /*  Set event-handler for filter function */
    set_filter_event_handler: function() {
        $("[name=filter-dropdown-item]").bind('click', function() {
            var filter_by = $(this).attr('filter-by');
            var current_filter_by = $(".filter-box[state=open]").attr('filter_by');

            $(".filter-box").hide();
            $(".filter-box").attr('state', 'close');

            if (current_filter_by === filter_by) {
                $(".filter-box[filter_by=" + filter_by + "]").hide();
            } else {
                $(".filter-box[filter_by=" + filter_by + "]").show();
                $(".filter-box[filter_by=" + filter_by + "]").attr('state', 'open');
            }
        });
    },
    /*  Set event-handler for search function */
    set_search_event_handler: function() {

    },
    /*  Set event-handler for search function */
    set_search_event_handler_old: function() {
        /**
         * @desc set event popover for "search form"
         */
        $('#search-icon').popover({
            trigger: 'click',
            placement: 'bottom',
            content: '<form action="#"><div class="input-group"><input type="text" placeholder="Search" class="form-control" name="search-keyword"><span class="input-group-btn"><button class="btn btn-' + class_bootsrap_component + '" type="submit" name="submit-search">Search</button></span></div></form>',
            html: true
        });

        /**
         * @desc on event "click" search-icon, set popover container to "visible-md" or "visible-lg"
         */
        $('#search-icon').bind('click', function(e) {
            e.preventDefault();
            $("[name=submit-search]").parents('.popover').addClass('visible-md').addClass('visible-lg');
            var keyword = $("[name=form-search] [name=search-keyword]").val();
            $("[name=search-keyword]").val(keyword);
        });

        /**
         * @desc on event "keyup" input text in "form-search", auto sync text
         */
        $('body').delegate('[name=search-keyword]', 'keyup', function(e) {
            e.preventDefault();
            var keyword = $(this).val();
            $('[name=search-keyword]').val(keyword);
        });


    },
    /*  Format number */
    formatNumber: function(number, decimal_digit, thousand_separator, decimal_separator) {
        decimal_digit = (typeof (decimal_digit) === "undefined") ? 0 : decimal_digit;
        thousand_separator = (typeof (thousand_separator) === "undefined") ? "," : thousand_separator;
        decimal_separator = (typeof (decimal_separator) === "undefined") ? "." : decimal_separator;
        return (typeof (accounting) === "undefined") ? number : accounting.formatNumber(parseInt(number), decimal_digit, thousand_separator, decimal_separator);
    },
    show_loading_animation: function(options) {
        var default_options = {
            css_background: {
                'width': '100%',
                'height': '100%',
                'background-color': '#000',
                'opacity': '0.8',
                'z-index': '9999',
                'position': 'fixed'
            },
            css_loading_box: {
                'position': 'absolute',
                'top': '50%',
                'left': '50%',
                'margin-left': '-15px'
            },
            css_loading_text: {
                'color': '#fff',
                'font-size': '18px',
                'font-family': 'sans-serif',
                'width': '150px',
                'letter-spacing': '1px',
                'display': 'block',
                'margin-left': '-60px',
                'text-align': 'center'
            },
            img: '<img src="' + _assets + 'img/loading.gif">',
            text_loading: 'Please wait'
        };

        options = (typeof (options) !== "undefined") ? $.extend(default_options, options) : default_options;

        //Generate loading HTML
        var loading_html = '<div id="bg-loading-animation">';
        loading_html = loading_html.concat(
                '<div class="loading-box">',
                options.img,
                '<br/>',
                '<span>' + options.text_loading + '</span>',
                '</div>',
                '</div>'
                );

        $('body').prepend(loading_html);

        $('body').find('#bg-loading-animation').css(options.css_background);
        $('body').find('#bg-loading-animation .loading-box').css(options.css_loading_box);
        $('body').find('#bg-loading-animation .loading-box span').css(options.css_loading_text);
    },
    hide_loading_animation: function() {
        $('body').find('#bg-loading-animation').remove();
    },
    alert_msg: function(options) {
        $.extend({
            type: null,
            dom_container: null,
            title: '',
            msg: '',
            fill_type: 'replace',
            show_title: false,
            auto_close: false,
            time_to_close: 5000
        }, options);

        var alert_class;
        switch (options.type) {
            case 'warning':
                alert_class = 'alert';
                break;
            case 'success' :
                alert_class = 'alert alert-success';
                break;
            case 'error' :
                alert_class = 'alert alert-danger';
                break;
            case 'info' :
                alert_class = 'alert alert-info';
                break;
            default :
                alert_class = 'alert alert-block';
                break;
        }

        var alert_title = (typeof (options.title) === "undefined" || options.show_title == false) ? '' : '<strong>' + options.title + '</strong>';
        switch (options.fill_type) {
            case 'append':
                if (options.dom_container.children('div.alert').length > 0) {
                    options.dom_container.children('div.alert').remove();
                }
                options.dom_container.append('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alert_title + ' ' + options.msg + '</div>');
                break;
            case 'prepend' :
                if (options.dom_container.children('div.alert').length > 0) {
                    options.dom_container.children('div.alert').remove();
                }
                options.dom_container.prepend('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alert_title + ' ' + options.msg + '</div>');
                break;
            case 'replace' :
                options.dom_container.html('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alert_title + ' ' + options.msg + '</div>');
                break;
            default :
                options.dom_container.html('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + options.title + ' ' + options.msg + '</div>');
                break;
        }

        if (options.auto_close === true) {
            setTimeout(function() {
                options.dom_container.find('.alert').remove();
            }, options.time_to_close);
        }
    },
    build_clean_url: function(url) {
        return url.replace(/&/g, "and").replace(/[^a-zA-Z0-9 _-]+/g, '').replace(/\s/gi, '-').replace(/--+/g, '-').toLowerCase();
    },
    scrollTop: function(dom_container, speed, parent_container) {
        dom_container = (typeof (dom_container) === "undefined") ? $("body") : dom_container;
        speed = (typeof (speed) === "undefined") ? 500 : speed;
        parent_container = (typeof (parent_container) === "undefined") ? $('html, body') : parent_container;
        parent_container.animate({
            scrollTop: dom_container.offset().top
        }, speed);
    },
    currency: function(price) {
        var currency_id = 2;
        var conversion_value = parseInt(13500);

        if (currency_id === 1)
        {
            var nominal = parseInt(price.replace(/\.\,\s/gi, ''));

            nominal = (nocurrenminal / conversion_value);
            var str = nominal.toFixed(10);
            //console.log(nominal + ' to '+ nominal.toFixed(2) + ' or '+ str.substring(0, str.length-8));
            var nilai_balik = Math.ceil(nominal * conversion_value);
            str = str.substring(0, str.length - 8);//get 2 digit comma no rounding.

            console.log('conversion : ' + conversion_value);
            console.log('IDR price : ' + price);
            console.log('US price : ' + str);
            console.log('US to IDR : ' + str + ' => ' + nilai_balik);

            return '$' + str.replace(/\./gi, ',');
        } else {
            return site.formatNumber(price, '', ',');
        }
    },
    refresh_toogle_cart_item: function() {
        $self = this;
        $.ajax({
            url: class_url + "refresh_toogle_cart_item",
            type: 'get',
            dataType: 'json',
            async: true,
            error: function(request) {
                console.log(request.responseText);
            },
            success: function(json) {
                $("#cart-block").html(json);
                $self.cart_info_update();
                $self.update_scroll_style();
            }
        });
    },
    cart_info_update: function() {
        $("#cart-info-count").html($("ul#cart-items li").length + " items");
        $("#cart-info-value").html($("#total_amount_cart").html());
    },
    set_modal_confirm_msg_event_handler: function() {
        var self = this;
        $("body").delegate("a.confirm-msg", "click", function(e) {
            e.preventDefault();
            self.show_confirm_message({msg: $(this).attr("data-msg"), href: $(this).attr("href")});
        });
    },
    show_confirm_message: function(options) {
        options = $.extend({
            msg: '',
            align: 'center',
            href: '#'
        }, options);
        $("#modal-confirmation-msg .modal-body").html('<div class="text-' + options.align + '">' + options.msg + '</div>');
        $("#modal-confirmation-msg [name=confirm-yes]").attr("href", options.href);
        $("#modal-confirmation-msg").modal("show");

    },
    blockUI_elm: function(elm) {
        elm.block({
            message: '<img src="' + _assets + 'img/loading-spinner-grey.gif">',
            css: {
                border: '0',
                padding: '0',
                background: 'none'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.2,
                cursor: 'wait'
            }
        });
    },
    // function to  un-block element(finish loading)
    unblockUI_elm: function(elm) {
        setInterval(function() {
            elm.unblock();
        }, 1000);
    }
};

$('document').ready(function() {
    site.init();
});
