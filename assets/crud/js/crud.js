var crud = {
    crud_modal_id: '#modal-crud', //Modal
    crud_form: '#modal-crud #crud-form', //Modal > Form
    crud_form_input_content: '#modal-crud .modal-body #input-form-content', //Modal > Form > Modal Body > #input-form-content : div container contained all form-group
    crud_btn_submit: '#modal-crud .modal-footer [type=submit]', //Modal > Form > Modal Footer > [type=submit]
    crud: function() {
        var self = this;

        //add "button new" to datatable container
        $("#datatable-container").prepend('<a href="#" data-action="add" data-group="crud-action" data-modal-submit-text="Save" data-modal-title="Add New Record" class="prevent-default btn btn-primary btn-action pull-right"><span class="glyphicon glyphicon-plus"></span>&nbsp;New</a>');

        //When button new|edit|remove|view in Datatable clicked
        $('body').delegate('[data-group=crud-action]', 'click', function(e) {
            e.preventDefault(e);
            self.load_crud_form($(this));
        });

        //When Form Submitted
        $('body').delegate(self.crud_form, 'submit', function(e) {
            e.preventDefault(e);
            $formData = new FormData($(this)[0]);
            self.process_crud_form($formData);
        });
    },
    load_crud_form: function(elm) {
        var self = this;

        //Set POST data parameter
        var data = {};
        if (elm.attr('data-action').match(/^(edit|remove)$/i)) {
            data[elm.attr('data-primary-field')] = elm.attr('data-primary-val'); //set parameter 'primary-id' as POST variable
        }

        //get FORM data by ajax
        $.ajax({
            url: class_url + elm.attr('data-action'),
            type: 'post',
            data: data,
            dataType: 'json',
            async: true,
            beforeSend: function() {
                $(self.crud_btn_submit).button('loading'); //disabled submit button
                $(self.crud_modal_id + " .loading-data-animation").show(); //show loading animation when loading the data
                $(self.crud_form_input_content).html(''); //clear all HTML input in form content
                $(self.crud_btn_submit).attr("action", elm.attr('data-action'));  //set ACTION attribute for SUBMIT BUTTON
                $(self.crud_btn_submit).html(elm.attr('data-modal-submit-text')); //set SUBMIT BUTTON button 'text'
                $(self.crud_modal_id + " .modal-title").html(elm.attr('data-modal-title')); //set modal title
                $(self.crud_modal_id).modal("show");  //at last : show modal
                $(self.crud_form).find(".alert").remove(); //remove alert message
            },
            error: function(request) {
                console.log(request.responseText);
            },
            success: function(json) {
                $(self.crud_modal_id + " .loading-data-animation").hide(); //hide loading animation when finish loaded the data

                if (json.status.match(/^success$/)) {
                    $(self.crud_btn_submit).button('reset'); //disabled submit button
                    $(self.crud_form_input_content).html(json.data); //fill the form with HTML input

                    //set datepicker
                    $(".datepicker").datepicker({
                        "dateFormat": "yy-mm-dd",
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-75:+0"
                    });

                    //set datetimepicker
                    $(".datetimepicker").datetimepicker({
                        timeFormat: 'HH:mm:ss',
                        dateFormat: "yy-mm-dd",
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-75:+0"
                    });

                    //set RTE : Richt Text Editor, TinyMCE
                    //tinymce.execCommand('mceRemoveControl', true, 'address_detail');
                    mytinymce.init({elements: "address_detail"});
                } else {
                    site.alert_msg({type: json.status, dom_container: $(self.crud_form_input_content), msg: json.msg, fill_type: 'append'});
                }
            }
        });
    },
    process_crud_form: function(data) {
        var self = this;

        //process Form POST data
        $.ajax({
            url: class_url + $(self.crud_btn_submit).attr('action'),
            type: 'post',
            data: data,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                $(self.crud_btn_submit).button('loading'); //loading button submit

                //remove all form-group class
                $form_group = $(self.crud_form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $form_group.find(".form-control-feedback").remove();
                $controls = $form_group.find(".controls");
                $controls.find(".text-danger").remove();
            },
            error: function(request) {
                $(self.crud_btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function(json) {
                $(self.crud_btn_submit).button('reset');
                if (json.status.match(/^success$/i)) {
                    //site.alert_msg({type: json.status, dom_container: $(self.crud_form_input_content), msg: json.msg, fill_type: 'append'});
                    site.alert_msg({type: json.status, dom_container: $(self.crud_modal_id + " .modal-footer"), msg: json.msg, fill_type: 'prepend'});
                    $(self.crud_modal_id + " .modal-footer .alert").addClass('text-left');
                } else {
                    site.alert_msg({type: json.status, dom_container: $(self.crud_modal_id + " .modal-footer"), msg: json.msg, fill_type: 'prepend'});
                    for (var field in json.form_error) {
                        $form_group = $(self.crud_form).find(".form-group[for=" + field + "]");
                        $controls = $form_group.find(".controls");
                        if (json.form_error[field] == "") { //if field SUCCESS
                            $form_group.addClass("has-feedback has-success");
                            $form_group.append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
                        } else {//if field ERROR
                            $form_group.addClass("has-feedback has-error");
                            $form_group.append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                            $controls.append(json.form_error[field]);
                        }
                    }
                }
            }
        });
    }
};

$.extend(site, crud);

$('document').ready(function() {
    site.crud();
});

