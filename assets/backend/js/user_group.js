var user_group = {
    user_group: function() {
        var self = this;
        var pathArray = window.location.pathname.split('/');
        if (pathArray[3] == 'add') {
            self.set_user_group_add();
        } else if (pathArray[3] == 'edit') {
            self.set_user_group_edit();
        }

        var oTable = $('#big_table').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": current_url + 'datatable',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "fnInitComplete": function(data) {
                //oTable.fnAdjustColumnSizing();
            },
            "columns": [
                {"data": "group_name"},
                {"data": "group_created"},
                {"data": "group_active"},
                {"data": "action"}
            ],
            'fnServerData': function(sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            }
        });

        self.set_user_group_delete();
    },
    set_user_group_add: function() {
        var self = this;
        self.clean_form("form-user-group-add");
        self.clean_form_reset("form-user-group-add");

        $("form#form-user-group-add").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.add_user_group(formData, 'form-user-group-add');
        });
    },
    set_user_group_edit: function() {
        var self = this;
        self.clean_form("form-user-group-edit");
        self.clean_form_reset("form-user-group-edit");

        $("form#form-user-group-edit").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.edit_user_group(formData, 'form-user-group-edit');
        });
    },
    set_user_group_delete: function() {
        var self = this;
        $(".data-action-delete").click(function(e) {
            e.preventDefault();
            var group_id = $(this).attr('data-id');
            $(".modal").modal('hide');
            $("#popup_box_del_confirmation").modal('show');
            $("#popup_box_del_confirmation p.content").html('Are you sure to delete ' + $(this).attr('data-name'));
            $("#popup_box_del_confirmation button.btn-del-conf-yes").click(function(e) {
                e.preventDefault();
                self.delete_user_group(group_id);
            });

            $("#popup_box_del_confirmation button.btn-del-conf-no").click(function(e) {
                e.preventDefault();
                $(".modal").modal('hide');
            });
        });
    },
    add_user_group: function(formData, formName) {
        var self = this;
        $.ajax({
            url: template_url + "user_group/add",
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                self.hide_loading_animation();
                if (json.status === 'success') {
                    $("#" + formName).append('<div class="alert alert-success success" role="alert">Success Updated</div>');
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field + "]");
                        var form_group = input_form.parents(".controls");

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }
                    if (json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
            }
        });
    },
    edit_user_group: function(formData, formName) {
        var self = this;
        $.ajax({
            url: template_url + "user_group/edit",
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                self.hide_loading_animation();
                if (json.status === 'success') {
                    $("#" + formName).append('<div class="alert alert-success success" role="alert">Success Updated</div>');
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field + "]");
                        var form_group = input_form.parents(".controls");

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }
                    if (json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
            }
        });
    },
    delete_user_group: function(id) {
        var self = this;
        $.ajax({
            url: template_url + "user_group/delete",
            type: 'post',
            data: {
                group_id: id
            },
            dataType: 'json',
            beforeSend: function() {
                self.show_loading_animation();
            },
            error: function(request) {
                self.hide_loading_animation();
                //$("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                self.hide_loading_animation();
                if (json.status === 'success') {
                    location.reload();
                } else {
                    $(".modal").modal('hide');

                }
            }
        });
    }
};

$.extend(site_backend, user_group);

$('document').ready(function() {
    site_backend.user_group();
});           