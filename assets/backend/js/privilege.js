var privilege = {
    privilege: function() {
        var self = this;
        self.set_privilege_event();
        self.set_privilege_save();

        $("#privilege-result").on( "click", ".checkall", function() {
            var checkedStatus = this.checked;
            var parent = $(this).parents('tr');
            parent.find('td :checkbox').each(function() {
                $(this).prop('checked', checkedStatus);
            });
        });
    },
    set_privilege_event: function() {
        var self = this;
        $("select#user_group_id").change(function() {
            self.load_data_privilege($(this).val());
        });
        self.clean_form("form-privilege");
        self.clean_form_reset("form-privilege");
    },
    set_privilege_save: function() {
        var self = this;
        $("form#form-privilege").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.save_privilege(formData, 'form-privilege');
        });
    },
    load_data_privilege: function(group_id) {
        var self = this;
        $.ajax({
            url: base_url + "backend/privilege/get_data",
            type: 'post',
            data: {
                group_id: group_id
            },
            dataType: 'json',
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form("form-privilege");
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#form-previlege").append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                self.hide_loading_animation();
                if (json.status == 'success') {
                    $("#privilege-result").html(json.data);
                } else {
                    $("#privilege-result").html('');
                }
            }
        });
    },
    save_privilege: function(formData, formName) {
        var self = this;
        $.ajax({
            url: base_url + "backend/privilege/save_privilege",
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                self.hide_loading_animation();
                if (json.status == 'success') {
                    self.load_data_privilege(json.group_id);
                    $("#" + formName).append('<div class="alert alert-success success" role="alert">Success Updated</div>');
                } else {
                    $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
                }
            }
        });
    }
};

$.extend(site_backend, privilege);

$('document').ready(function() {
    site_backend.privilege();
});           