/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var article = {
    article: function() {
        var self = this;

        self.showRecaptcha("recaptcha_div");
        self.set_comment_event();

        $('.box-slider2').flexslider({
            slideshow: false,
            animation: "slide",
            controlNav: false,
            itemWidth: 300,
            itemMargin: 30,
            minItems: self.getGridSize('box-slider2'),
            maxItems: self.getGridSize('box-slider2')
        });

        $(window).resize(function(e) {
            e.preventDefault();
            $('.box-slider2').data('flexslider').vars.minItems = self.getGridSize('box-slider2');
	    $('.box-slider2').data('flexslider').vars.maxItems = self.getGridSize('box-slider2');
	});
    },

    getGridSize: function(type) {
        return (window.innerWidth < 500) ? 1 : 2;
    },
    
    set_comment_event: function() {
        var self = this;
        $("form#form-comment-article").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.send_comment(formData, 'form-comment-article');
        });
    },
            
    send_comment: function(formData, formName) {
        var self = this;
        var query_string = typeof(GET) !== "undefined" ? "?" +$.param(GET) : "";
        $.ajax({
            url: base_url + "blog/send_comment" + query_string,
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
                Recaptcha.reload();
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                if(json.status === 'success') {
                    $("#" + formName).append(json.form_success);
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field +"], " + "#" + formName + " textarea[name=" + field +"]");
                        var form_group = input_form.parent();

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }

                    if(json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
                self.hide_loading_animation();
            }
        });
    }

};

$.extend(site_frontend, article);

$('document').ready(function() {
    site_frontend.article();
});

                    