var register = {
    register: function() {
        var self = this;
        self.set_register_event();
    },

    set_register_event: function() {
        var self = this;
        $("form#form-register").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.register_member(formData, 'form-register');
        });
    }
};

$.extend(site_frontend, register);

$('document').ready(function() {
    site_frontend.register();
});


