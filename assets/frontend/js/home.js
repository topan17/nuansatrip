var home = {
    home: function() {
        var self = this;

        $('.banner-box-slider').flexslider({
            animation: "slide",
            controlNav: false
        });

        $('.box-slider1').flexslider({
            slideshow: false,
            animation: "slide",
            controlNav: false,
            itemWidth: 300,
            itemMargin: 30,
            minItems: self.getGridSize('box-slider1'),
            maxItems: self.getGridSize('box-slider1')
        });

        $('.box-slider2').flexslider({
            slideshow: false,
            animation: "slide",
            controlNav: false,
            itemWidth: 300,
            itemMargin: 30,
            minItems: self.getGridSize('box-slider2'),
            maxItems: self.getGridSize('box-slider2')
        });

        $('.box-slider3').flexslider({
            slideshow: false,
            animation: "slide",
            controlNav: false,
            itemWidth: 300,
            itemMargin: 30,
            minItems: self.getGridSize('box-slider3'),
            maxItems: self.getGridSize('box-slider3')
        });

        $(window).resize(function(e) {
            e.preventDefault();
//	    $('.box-slider1').data('flexslider').vars.minItems = self.getGridSize('box-slider1');
//	    $('.box-slider1').data('flexslider').vars.maxItems = self.getGridSize('box-slider1');
            $('.box-slider2').data('flexslider').vars.minItems = self.getGridSize('box-slider2');
	    $('.box-slider2').data('flexslider').vars.maxItems = self.getGridSize('box-slider2');
//            $('.box-slider3').data('flexslider').vars.minItems = self.getGridSize('box-slider3');
//	    $('.box-slider3').data('flexslider').vars.maxItems = self.getGridSize('box-slider3');
	});

        $(".banner-box-slider ul.flex-direction-nav").hide();
        $(".banner-box-slider").hover(function () {
            $(".banner-box-slider ul.flex-direction-nav").fadeToggle();
        });
    },

    getGridSize: function(type) {
        if(type.match(/^box-slider1/i)) {
            return (window.innerWidth < 450) ? 1 : (window.innerWidth < 767) ? 2 : (window.innerWidth < 991) ? 3 : 4 ;
        } else if(type.match(/^box-slider2/i)) {
            return (window.innerWidth < 500) ? 1 : (window.innerWidth < 991) ? 2 : 3 ;
        } else if(type.match(/^box-slider3/i)) {
            return (window.innerWidth < 450) ? 1 : (window.innerWidth < 767) ? 2 : (window.innerWidth < 991) ? 3 : 4 ;
        }
    }
};

$.extend(site_frontend, home);

$('document').ready(function() {
    site_frontend.home();
});
