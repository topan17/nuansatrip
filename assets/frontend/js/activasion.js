/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var activasion = {
    activasion: function() {
        var self = this;
        self.set_activasion_event();
    },

    set_activasion_event: function() {
        var self = this;
        $("form#form-activasion").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.request_activasion(formData, 'form-activasion');
        });
    }
};

$.extend(site_frontend, activasion);

$('document').ready(function() {
    site_frontend.activasion();
});

                    