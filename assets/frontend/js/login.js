var login = {
    login: function() {
        var self = this;
        self.set_login_event();
    },

    set_login_event: function() {
        var self = this;
        $("form#form-login").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.login_member(formData, 'form-login');
        });
    }
};

$.extend(site_frontend, login);

$('document').ready(function() {
    site_frontend.login();
});

