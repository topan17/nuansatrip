var contactus = {
    contactus: function() {
        var self = this;
        self.showRecaptcha("recaptcha_div");
        self.set_contactus_event();
    },

    set_contactus_event: function() {
        var self = this;
        $("form#form-contactus").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.send_message(formData, 'form-contactus');
        });
    },
            
    send_message: function(formData, formName) {
        var self = this;
        var query_string = typeof(GET) !== "undefined" ? "?" +$.param(GET) : "";
        $.ajax({
            url: base_url + "contact-us/send_message" + query_string,
            type: 'post',
            data: formData,
            dataType: 'json',
            async: false,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                self.show_loading_animation();
                self.clean_form(formName);
                Recaptcha.reload();
            },
            error: function(request) {
                self.hide_loading_animation();
                $("#" + formName).append('<div class="alert alert-danger error" role="alert">Something wrong, please try again</div>');
            },
            success: function(json) {
                if(json.status === 'success') {
                    $("#" + formName).append(json.form_success);
                } else {
                    for (var field in json.form_error) {
                        var input_form = $("#" + formName + " input[name=" + field +"], " + "#" + formName + " textarea[name=" + field +"]");
                        var form_group = input_form.parent();

                        form_group.append(json.form_error[field]);
                        input_form.addClass("error");
                    }

                    if(json.form_error.show_default === true) {
                        $("#" + formName).append(json.form_error.default_error);
                    }
                }
                self.hide_loading_animation();
            }
        });
    }
};

$.extend(site_frontend, contactus);

$('document').ready(function() {
    site_frontend.contactus();
});