/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var forgot_password = {
    forgot_password: function() {
        var self = this;
        self.set_forgot_password_event();
    },

    set_forgot_password_event: function() {
        var self = this;
        $("form#form-forgotpass").submit(function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            self.request_forgotpass(formData, 'form-forgotpass');
        });
    }
};

$.extend(site_frontend, forgot_password);

$('document').ready(function() {
    site_frontend.forgot_password();
});

                    