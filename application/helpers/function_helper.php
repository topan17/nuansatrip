<?php

/**
 * 
 * @desc this function used to array $options, contain dropdown-data used for form-helper-function : form_dropdown(array)
 * @param $arr array source
 * @param $field_key field-name which it value will be used as key-value in $options
 * @param $field_val field-name which it value will be used as value in $options
 * @desc for example view this code : http://localhost/core/kliktodaycms/bank_payment
 * @return array
 * @Auth : Harry Osmmar Sitohang 2012-10-10
 * 
 */
function create_form_dropdown_options($arr = array(), $field_key = '', $field_val = '') {
    $options = array();
    foreach ($arr as $field) {
        $options[$field[$field_key]] = $field[$field_val];
    }
    return $options;
}

/**
 * 
 * @desc this function used to format date to default format
 * @param $date date
 * @param $format with default value 'd M Y' => '01 Jan 2012'
 * @return date-formated
 * @Auth : Harry Osmmar Sitohang 2012-10-25
 * 
 */
function format_date($date, $format = 'd M Y') {
    return date($format, strtotime($date));
}

/**
 * 
 * @desc this function used to format number to default number
 * @param $number number
 * @param $format number
 * @return number-formated
 * 
 */
function format_number($number) {
    return number_format(!empty($number) ? $number : 0, 0, '.', ',');
}

function generate_breadcrumb_title($title) {
    return ucwords(preg_replace("/[-_]/", " ", $title));
}

function generate_error_form_validation($field_form = array(), $default_error = '', $show_default = FALSE) {
    foreach ($field_form as $row => $field) {
        if ((form_error($field['field']))) {
            $array_err[$field['field']] = form_error($field['field'], '<div class="alert alert-danger error" role="alert">', '</div>');
        }
    }
    $array_err['default_error'] = $default_error;
    $array_err['show_default'] = $show_default;
    return $array_err;
}

function get_random_str($length = 5, $allowed_char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= $allowed_char[mt_rand(0, strlen($allowed_char) - 1)];
    }

    return $string;
}

function get_friendly_url($str) {
    // Remove any character that is not alphanumeric, white-space, or a hyphen
    $str = preg_replace("/[^a-z0-9\s\-]/i", "", $str);
    // Replace multiple instances of white-space with a single space
    $str = preg_replace("/\s\s+/", " ", $str);
    // Replace all spaces with hyphens
    $str = preg_replace("/\s/", "-", $str);
    // Replace multiple hyphens with a single hyphen
    $str = preg_replace("/\-\-+/", "-", $str);
    // Remove leading and trailing hyphens
    $str = preg_replace("/^\-|\-$/", "", $str);
    // Lowercase the URL
    $str = strtolower($str);

    return $str;
}

/**
 *
 * @desc this function used to encrypt password
 * @param string $password
 * @return password encrypted
 *
 */
function encrypt_password($password){
    $CI = &get_instance();
    $encryption_key = $CI->config->item("encryption_key");
    unset($CI);
    return sha1("{$encryption_key}{$password}");
}

/* End of file function_helper.php */
/* Location: ./application/helper/function_helper.php */
