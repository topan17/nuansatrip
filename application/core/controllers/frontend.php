<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/core.php';
include_once APPPATH . 'third_party/recaptchalib.php';

/**
 * @author : Harry Osmar Sitohang
 * @date : 30 Jun 2014
 * @desc : This class used as core, all frontend controller should be extend this class
 */
class Frontend extends Core {

    protected $template_path = 'frontend';
    protected $_category_blog;
    protected $_count_article;

    public function __construct() {
        parent::__construct();
        //set pages meta
        $this->set_pages_meta();
        if($this->uri->segment('1') === 'blog'){
            $this->init_blog();
        }
    }

    public function set_pages_meta() {
        $this->load->model('pages_model', 'pages');
        $query = $this->pages->_get(array("page_url" => uri_string()));
        if ($query->num_rows > 0) {
            $this->pages_meta['title'] = $query->row()->page_title;
            $this->pages_meta['desc'] = $query->row()->page_desc;
            $this->pages_meta['keywords'] = $query->row()->page_keywords;
        } else {
            $query = $this->pages->_get(array("page_url" => 'home'));
            $this->pages_meta['title'] = $query->row()->page_title;
            $this->pages_meta['desc'] = $query->row()->page_desc;
            $this->pages_meta['keywords'] = $query->row()->page_keywords;
        }

        $this->view->set('pages_meta', $this->pages_meta);
    }

    protected function init_blog() {
        $this->page_css[] = "{$this->_assets_css}blog.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";
        $this->page_js[] = "{$this->_assets_js}blog.js";

        $this->view->set("pages_css", $this->page_css);
        $this->view->set("pages_js", $this->page_js);

        //get category blog
        $this->get_category_blog();
        $this->view->set('category_blog', $this->_category_blog);

        $this->count_article_blog();
        $this->view->set('count_article', $this->_count_article->num_rows());
    }

    public function get_category_blog() {
        $this->load->model('blog_category_model', 'blog_category');
        $this->_category_blog = $this->blog_category->_get_count_article();
    }

    public function count_article_blog() {
        $this->load->model('blog_article_model', 'blog_article');
        $this->_count_article = $this->blog_article->_get(array('article_active' => 'YES'));
    }

    function logout() {
        $array_items = array(
            'is_login' => '',
            'member_id' => '',
            'member_name' => '',
            'member_email' => ''
        );
        $this->session->unset_userdata($array_items);
        if (!empty($_GET['urlgoto'])) {
            redirect($_GET['urlgoto']);
        } else {
            redirect(base_url());
        }
    }

    function sendmail($type, $data, $email) {
        $this->load->library('email');
        $this->email->initialize($this->config->item('sendmail'));

        switch ($type) {
            case "register":
                $email_subject = "Email aktivasi | Terima kasih telah bergabung di nuansatrip.com";
                $template = $this->view->load('email_temp/activasion', $data, true);
                break;
            case "forgotpass":
                $email_subject = "Password Baru | nuansatrip.com";
                $template = $this->view->load('email_temp/forgotpass', $data, true);
                break;
            case "contactus":
                $email_subject = "Contact us | ".$data['contactus_name'];
                $template = $this->view->load('email_temp/contactus', $data, true);
                break;
            case "temp2":
                echo "temp2";
                break;
            default:
                echo "default temp";
        }

        $this->email->set_mailtype("html");
        $this->email->from('nuansatrip@gmail.com', 'Nuansa Trip');
        $this->email->to($email);

        $this->email->subject($email_subject);
        $this->email->message($template);

        $this->email->send();

        return TRUE;
    }

    function check_recaptcha($response_field) {
        $resp = recaptcha_check_answer(
                        $this->config->item('recaptcha_private_key'),
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["recaptcha_challenge_field"],
                        $response_field
        );

        $test = 'form_validation';
        if (!$resp->is_valid) {
            $this->{$test}->set_message('check_recaptcha', 'Angka atau huruf tidak sesuai');
            return false;
        } else {
            return true;
        }
    }

    function generate_session_login($data) {
        $this->load->model('member_model', 'member');
        $data_session = array(
            'is_login' => 'YES',
            'member_id' => $data['member_id'],
            'member_name' => $data['member_name'],
            'member_email' => $data['member_email']
        );
        $data_update = array('member_last_login' => date('Y-m-d h:i:s'));
        $this->member->_update($data_update, array('member_id' => $data['member_id']));
        $this->session->set_userdata($data_session);
    }

}

/* End of file frontend.php */
/* Location: ./application/core/controllers/frontend.php */