<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/backend.php';

class Privilege extends Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model("User_group_model");

        $this->page_js[] = "{$this->_assets_js}privilege.js";

        $this->view->set(array(
            'user_groups' => $this->User_group_model->_get()->result()
        ));

        parent::index();
    }

    public function get_data() {
        if ($this->input->is_ajax_request()) {
            if ($_POST['group_id'] > 0) {
                $this->load->model("Privilege_model");
                $data_privilege = $this->Privilege_model->generate_privilege_data($_POST['group_id']);
                $json = array(
                    'status' => 'success',
                    'data' => $this->generate_privilege_html($data_privilege)
                );
            } else {
                $json = array(
                    'status' => 'error',
                );
            }

            echo json_encode($json);
            die();
        } else {
            redirect($this->template_url . 'login');
        }
    }

    public function save_privilege() {
        if ($this->input->is_ajax_request()) {
            if ($_POST['user_group_id'] > 0 && !empty($_POST['menu_check'])) {
                
                /*DELETE TO RESET PRIVILEGE GROUP*/
                $this->db->delete('privilege', array('group_id' => $_POST['user_group_id']));
                
                /*INSERT NEW OR UPDATE GROUP PRIVILEGE*/
                foreach($_POST['menu_check'] as $key => $val){
                    $data = array(
                        'group_id' => $_POST['user_group_id'], 
                        'menu_backend' => $key,
                        'privilege_action' => implode(',', $val)
                    );
                    $this->db->insert('privilege', $data); 
                }
                
                $json = array(
                    'group_id' =>$_POST['user_group_id'],
                    'status' => 'success'
                );
            } else {
                $json = array(
                    'status' => 'error',
                );
            }
            echo json_encode($json);
            die();
        } else {
            redirect($this->template_url . 'login');
        }
    }

    protected function generate_privilege_html(array $array, $padding = 0) {
        $html = '';
        foreach ($array as $value => $list) {
            $checkbox = '';
            $count_checkbox = count(explode(',', $list['menu_act']));
            $count_check = 0;
            if (!empty($list['act_view'])) {
                $checked = ($list['act_view'] == 'YES') ? 'checked' : '';
                $checkbox .= '<td><label class="checkbox-inline"><input type="checkbox" name="menu_check['.$list['menu_id'].'][]" value="view" ' . $checked . '>VIEW</label></td>';
                if ($checked == 'checked') :
                    $count_check++;
                endif;
            } else {
                $checkbox .= '<td><label class="checkbox-inline">&nbsp;</label></td>';
            }

            if (!empty($list['act_add'])) {
                $checked = ($list['act_add'] == 'YES') ? 'checked' : '';
                $checkbox .= '<td><label class="checkbox-inline"><input type="checkbox" name="menu_check['.$list['menu_id'].'][]" value="add" ' . $checked . '>ADD</label></td>';
                if ($checked == 'checked') :
                    $count_check++;
                endif;
            } else {
                $checkbox .= '<td><label class="checkbox-inline">&nbsp;</label></td>';
            }

            if (!empty($list['act_edit'])) {
                $checked = ($list['act_edit'] == 'YES') ? 'checked' : '';
                $checkbox .= '<td><label class="checkbox-inline"><input type="checkbox" name="menu_check['.$list['menu_id'].'][]" value="edit" ' . $checked . '>EDIT</label></td>';
                if ($checked == 'checked') :
                    $count_check++;
                endif;
            } else {
                $checkbox .= '<td><label class="checkbox-inline">&nbsp;</label></td>';
            }

            if (!empty($list['act_delete'])) {
                $checked = ($list['act_delete'] == 'YES') ? 'checked' : '';
                $checkbox .= '<td><label class="checkbox-inline"><input type="checkbox" name="menu_check['.$list['menu_id'].'][]" value="delete" ' . $checked . '>DELETE</label></td>';
                if ($checked == 'checked') :
                    $count_check++;
                endif;
            } else {
                $checkbox .= '<td><label class="checkbox-inline">&nbsp;</label></td>';
            }

            if (!empty($list['act_download'])) {
                $checked = ($list['act_download'] == 'YES') ? 'checked' : '';
                $checkbox .= '<td><label class="checkbox-inline"><input type="checkbox" name="menu_check['.$list['menu_id'].'][]" value="download" ' . $checked . '>DOWNLOAD</label></td>';
                if ($checked == 'checked') :
                    $count_check++;
                endif;
            } else {
                $checkbox .= '<td><label class="checkbox-inline">&nbsp;</label></td>';
            }

            $allcheck = ($count_check == $count_checkbox) ? "checked" : "";

            if (!empty($list['submenu'])) {
                if (($list['menu_parent'] == 0)) {
                    $html .= '<tr>';
                    $html .= '<td>';
                    $html .= '<label><i class="fa fa-angle-double-right icon"></i> ' . (!empty($list['menu_icon']) ? '<i class="fa ' . $list['menu_icon'] . ' icon"></i> ' : "") . strtoupper($list['menu_name']) . '</label>';
                    $html .= '</td>';
                    $html .= $checkbox;
                    $html .= '<td><label class="checkbox-inline"><input type="checkbox" name="checkall" class="checkall" ' . $allcheck . '>ALL</label></td>';
                    $html .= '</tr>';
                } else {
                    $html .= '<tr>';
                    $html .= '<td style="padding-left:' . $padding . 'px;">';
                    $html .= '<label><i class="fa fa-angle-double-right icon"></i> ' . strtoupper($list['menu_name']) . '</label>';
                    $html .= '</td>';
                    $html .= $checkbox;
                    $html .= '<td><label class="checkbox-inline"><input type="checkbox" name="checkall" class="checkall" ' . $allcheck . '>ALL</label></td>';
                    $html .= '</tr>';
                }
                $html .= $this->generate_privilege_html($list['submenu'], $padding + 15);
            } else {
                if (($list['menu_parent'] == 0)) {
                    $html .= '<tr>';
                    $html .= '<td>';
                    $html .= '<label><i class="fa fa-angle-double-right icon"></i> ' . (!empty($list['menu_icon']) ? '<i class="fa ' . $list['menu_icon'] . ' icon"></i> ' : "") . strtoupper($list['menu_name']) . '</label>';
                    $html .= '</td>';
                    $html .= $checkbox;
                    $html .= '<td><label class="checkbox-inline"><input type="checkbox" name="checkall" class="checkall" ' . $allcheck . '>ALL</label></td>';
                    $html .= '</tr>';
                } else {
                    $html .= '<tr>';
                    $html .= '<td style="padding-left:' . $padding . 'px;">';
                    $html .= '<label><i class="fa fa-angle-double-right icon"></i> ' . strtoupper($list['menu_name']) . '</label>';
                    $html .= '</td>';
                    $html .= $checkbox;
                    $html .= '<td><label class="checkbox-inline"><input type="checkbox" name="checkall" class="checkall" ' . $allcheck . '>ALL</label></td>';
                    $html .= '</tr>';
                }
            }
        }
        return $html;
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/backend/dashboard.php */