<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/backend.php';

class Login extends Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->page_js[] = "{$this->_assets_js}login.js";
        $this->page_css[] = "{$this->_assets_css}login.css";
        parent::index();
    }
}

/* End of file login.php */
/* Location: ./application/controllers/backend/login.php */