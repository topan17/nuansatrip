<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/backend.php';

class User_group extends Backend {

    protected $data_table;
    protected $per_page = 2;

    public function __construct() {
        parent::__construct();
        $this->load->model('user_group_model', 'user_group');
        $this->page_css[] = "{$this->_assets_css}user_group.css";
        $this->page_js[] = "{$this->_assets_js}user_group.js";
        $this->load->library('Datatables');
        $this->load->library('table');
    }

//    public function index() {        
//        $this->data_table = $this->user_group->_generate_grid_table($this->per_page);
//        $this->view->set("data_table", $this->data_table);
//        $this->view->set("data_action", $this->privilege_action);
//        $this->view->set("data_paging", $this->create_paging());
//
//        parent::index();
//    }
    
    function index(){
        //set table id in table open tag
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="mytable">');
        $this->table->set_template($tmpl);
 
        $this->table->set_heading('GROUP NAME', 'GROUP CREATED', 'GROUP ACTIVE', 'ACTION');
        //$this->table->add_row(array('Group Name', 'Group Created', 'group active'));
 
        //$this->load->view('pages/user_group');
        parent::index();
    }
 
    //function to handle callbacks
    function datatable() {
//        $_POST['start'] = $_POST['iDisplayStart'];
//        $_POST['length'] = $_POST['iDisplayLength'];
//        if(!empty($_POST['sSearch']) && isset($_POST['sSearch'])){
//            $_POST['search'] = $_POST['sSearch'];
//        }
        
        $this->datatables->select('group_id,group_name,group_created,group_active')
            ->unset_column('group_id')
//            ->add_column('avatar', '<img src="$1"/>', 'avatar')
//            ->add_column('Actions', get_buttons('$1'), 'id')
            ->add_column('action', '<a href="profiles/edit/$1">EDIT</a>', 'group_id')
            ->from('user_group');
 
        echo $this->datatables->generate();
    }
    

    public function add() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('add_user_group'), "add_user_group");
            if ($this->add_user_group->run()) {
                $data = array(
                    'group_name' => strtoupper($this->input->post('group_name')),
                    'group_created' => date('Y-m-d h:i:s'),
                    'group_active' => $this->input->post('group_active')
                );

                $query = $this->user_group->_insert($data);

                if ($query === true) {
                    $msg = '<div class="alert alert-success success" role="alert">Success Updated</div>';
                    $json_encode = array(
                        'status' => 'success',
                        'msg' => $msg
                    );
                    echo json_encode($json_encode);
                    die();
                } else {
                    $err = '<div class="alert alert-danger error" role="alert">Database Error</div>';
                    $json_encode = array(
                        'status' => 'error',
                        'form_error' => generate_error_form_validation(array(), $err, TRUE)
                    );
                    echo json_encode($json_encode);
                    die();
                }
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('add_user_group'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            $this->view->set("pages_css", $this->page_css);
            $this->view->set("pages_js", $this->page_js);

            $this->view->content("pages/user_group-add");
        }
    }

    public function edit($id = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('edit_user_group'), "edit_user_group");
            if ($this->edit_user_group->run()) {
                $data = array(
                    'group_name' => strtoupper($this->input->post('group_name')),
                    'group_active' => $this->input->post('group_active')
                );

                $query = $this->user_group->_update($data, array('group_id' => $this->input->post('group_id')));

                if ($query === true) {
                    $msg = '<div class="alert alert-success success" role="alert">Success Updated</div>';
                    $json_encode = array(
                        'status' => 'success',
                        'msg' => $msg
                    );
                    echo json_encode($json_encode);
                    die();
                } else {
                    $err = '<div class="alert alert-danger error" role="alert">Database Error</div>';
                    $json_encode = array(
                        'status' => 'error',
                        'form_error' => generate_error_form_validation(array(), $err, TRUE)
                    );
                    echo json_encode($json_encode);
                    die();
                }
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('edit_user_group'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            $this->view->set("pages_css", $this->page_css);
            $this->view->set("pages_js", $this->page_js);
            $data_form = $this->user_group->_get(array('group_id' => $id));
            if ($data_form->num_rows() > 0) {
                $this->view->set("data_form", $data_form);
                $this->view->content("pages/user_group-edit");
            } else {
                redirect($this->class_url);
            }
        }
    }

    public function delete() {
        if ($this->input->is_ajax_request()) {
            $group_id = $this->input->post('group_id');
            $this->user_group->_delete(array('group_id' => $group_id));
            if ($this->db->affected_rows() == TRUE) {
                $this->session->set_flashdata('success', 'Deleted success');
                $json_encode = array(
                    'status' => 'success'
                );
                echo json_encode($json_encode);
                die();
            } else {
                $this->session->set_flashdata('error', 'Deleted error');
                $json_encode = array(
                    'status' => 'error'
                );
                echo json_encode($json_encode);
                die();
            }
        }
    }

    public function check_add_group_name($group_name) {
        $this->load->model('user_group_model', 'user_group');
        $query = $this->user_group->_get(array("group_name" => $group_name));

        if ($query->num_rows > 0) {
            $this->add_user_group->set_message('check_add_group_name', 'Nama group sudah digunakan');
            return false;
        } else {
            return true;
        }
    }

    public function check_edit_group_name($group_name) {
        $group_id = $this->input->post('group_id');
        $this->load->model('user_group_model', 'user_group');
        $query = $this->user_group->_get(array("group_name" => $group_name, "group_id !=" => $group_id));

        if ($query->num_rows > 0) {
            $this->edit_user_group->set_message('check_edit_group_name', 'Nama group sudah digunakan');
            return false;
        } else {
            return true;
        }
    }

    public function create_paging() {
        $this->load->library('pagination');
        $temp_GET = $_GET;
        if (isset($temp_GET['per_page'])) {
            unset($temp_GET['per_page']);
        }

//        if (!isset($temp_GET['sort'])) {
//            $temp_GET['sort'] = 'product_new';
//        }
//
//        if (!isset($temp_GET['by'])) {
//            $temp_GET['by'] = 'desc';
//        }

        $url_get = http_build_query($temp_GET);
        $data['total_rows'] = $this->user_group->_get()->num_rows();
        $this->pagination->initialize(array(
            'base_url' => sprintf("%s?%s", current_url(), $url_get),
            'total_rows' => $data['total_rows'],
            'per_page' => $this->per_page,
            'page_query_string' => TRUE,
            'first_link' => 'First',
            'last_link' => 'Last',
            'next_link' => 'Next',
            'prev_link' => 'Prev',
            'cur_tag_open' => '<li><a class="active">',
            'cur_tag_close' => '</a></li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>'
        ));
        $data['page_count'] = ceil($data['total_rows'] / $this->per_page);
        return $this->pagination->create_links();
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/backend/dashboard.php */