<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/backend.php';

class Dashboard extends Backend {

    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        parent::index();
    }
}

/* End of file dashboard.php */
/* Location: ./application/controllers/backend/dashboard.php */