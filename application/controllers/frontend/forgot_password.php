<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Forgot_password extends Frontend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->page_css[] = "{$this->_assets_css}forgot_password.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";

        $this->page_js[] = "{$this->_assets_js}forgot_password.js";


        parent::index();
    }

    function request_forgotpass() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('request_forgotpass'), "request_forgotpass");
            $this->load->model('member_model', 'member');

            if ($this->request_forgotpass->run()) {
                /* generate new pass */
                $new_pass = get_random_str(5);
                $password = $new_pass;
                $salt = sha1(md5($password));
                $password = md5($password . $salt);

                $where = array('member_email' => $this->input->post('email_address'));
                $data = array(
                    'member_salt' => $salt,
                    'member_password' => $password
                );

                $this->member->_update($data, $where);

                $query = $this->member->_get(array("member_email" => $this->input->post('email_address')));

                $data_member = $query->row_array();
                $data_member['new_pass'] = $new_pass;

                $this->sendmail('forgotpass', $data_member, $this->input->post('email_address'));
                $msg = '<div class="alert alert-success success" role="alert">Silahkan cek email anda, kami telah mengirim password baru ke email ' . $query->row()->member_email . '</div>';
                $json_encode = array(
                    'status' => 'success',
                    'member_email' => $this->input->post('email_address'),
                    'form_success' => $msg
                );
                echo json_encode($json_encode);
                die();
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('request_forgotpass'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            redirect(base_url('forgot_password'));
        }
    }

    function check_email_forgotpass($email) {
        $this->load->model('member_model', 'member');
        $query = $this->member->_get(array("member_email" => $email));

        if ($query->num_rows === 1) {
            if ($query->row()->member_active === 'YES') {
                return true;
            } else {
                $this->request_forgotpass->set_message('check_email_forgotpass', 'Alamat email belum aktif, silahkan aktivasi disini <a href="' . base_url('activasion') . '">aktivasi</a>');
                return false;
            }
        } else {
            $this->request_forgotpass->set_message('check_email_forgotpass', 'Alamat email belum terdaftar');
            return false;
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */