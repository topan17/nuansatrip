<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Register extends Frontend {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('is_login') == 'YES') {
            redirect(base_url());
        }
    }

    public function index() {
        $this->page_css[] = "{$this->_assets_css}register.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";

        $this->page_js[] = "{$this->_assets_js}register.js";

        parent::index();
    }

    public function register_member() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('register'), "register_form");
            if ($this->register_form->run()) {
                $this->load->model('member_model', 'member');
                $password = $this->input->post('member_password');
                $salt = sha1(md5($password));
                $password = md5($password . $salt);

                $data_member = array(
                    'member_name' => $this->input->post('member_name'),
                    'member_email' => $this->input->post('member_email'),
                    'member_salt' => $salt,
                    'member_password' => $password,
                    'member_created' => date('Y-m-d h:i:s'),
                    'activasion_code' => md5(mt_rand() . date('Y-m-d h:i:s') . mt_rand()),
                    'activasion_expired' => date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s') . ' + 1 day'))
                );

                $query = $this->member->_insert($data_member);
                if ($query === true) {
                    $this->sendmail('register', $data_member, $this->input->post('member_email'));

                    $json_encode = array(
                        'status' => 'success',
                        'member_email' => $data_member['member_email']
                    );
                    echo json_encode($json_encode);
                    die();
                } else {
                    $err = '<div class="alert alert-danger error" role="alert">Database Error</div>';
                    $json_encode = array(
                        'status' => 'error',
                        'form_error' => generate_error_form_validation(array(), $err, TRUE)
                    );
                    echo json_encode($json_encode);
                    die();
                }
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('register'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            redirect(base_url('register'));
        }
    }

    function check_register_email($email) {
        $this->load->model('member_model', 'member');
        $query = $this->member->_get(array("member_email" => $email));

        if ($query->num_rows > 0) {
            $this->register_form->set_message('check_register_email', 'Alamat email sudah digunakan');
            return false;
        } else {
            return true;
        }
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */