<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Contactus extends Frontend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->page_css[] = "{$this->_assets_css}contactus.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";

        $this->page_js[] = "{$this->_assets_js}contactus.js";


        parent::index();
    }

    public function send_message() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('contactus'));
            if ($this->form_validation->run()) {
                $this->load->model('contactus_model', 'contactus');
                $msg = strip_tags($this->input->post('contactus_message', true));
                $msg = $this->security->xss_clean($msg);

                $data_contactus = array(
                    'contactus_email' => $this->input->post('contactus_email'),
                    'contactus_name' => $this->input->post('contactus_name'),
                    'contactus_message' => $msg,
                    'contactus_created' => date('Y-m-d h:i:s')
                );

                $query = $this->contactus->_insert($data_contactus);
                if ($query === true) {
                    $this->sendmail('contactus', $data_contactus, 'nuansatrip@gmail.com');
                    $msg = '<div class="alert alert-success success" role="alert">Pesan sudah disimpan.</div>';
                    $json_encode = array(
                        'status' => 'success',
                        'form_success' => $msg
                    );
                    echo json_encode($json_encode);
                    die();
                } else {
                    $err = '<div class="alert alert-danger error" role="alert">Database Error</div>';
                    $json_encode = array(
                        'status' => 'error',
                        'form_error' => generate_error_form_validation(array(), $err, TRUE)
                    );
                    echo json_encode($json_encode);
                    die();
                }
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('contactus'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            redirect(base_url('contact-us'));
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */