<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Login extends Frontend {

    public function __construct() {
        parent::__construct();
        if($this->session->userdata('is_login') == 'YES'){
            redirect(base_url());
        }
    }

    public function index() {
        $this->page_css[] = "{$this->_assets_css}login.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";
       
        $this->page_js[] = "{$this->_assets_js}login.js";
        
        parent::index();
    }

    public function login_member() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('login_form'), "login_form");
            if ($this->login_form->run()) {
                $this->load->model('member_model', 'member');
                $query = $this->member->_get(array('member_email' => $this->input->post('login_email')));
                $salt = $query->row()->member_salt;
                $password = md5($this->input->post('login_password') . $salt);
                if ($password == $query->row()->member_password) {
                    $this->generate_session_login($query->row_array());
                    $msg = '<div class="alert alert-success success" role="alert">Login sukses</div>';
                    $json_encode = array(
                        'status' => 'success',
                        'form_success' => $msg,
                        'urlgoto' => !empty($_GET['urlgoto']) ? $_GET['urlgoto'] : ''
                    );
                    echo json_encode($json_encode);
                    die();
                } else {
                    $err = '<div class="alert alert-danger error" role="alert">Email atau password tidak sesuai</div>';
                    $json_encode = array(
                        'status' => 'error',
                        'form_error' => generate_error_form_validation(array(), $err, TRUE)
                    );
                    echo json_encode($json_encode);
                    die();
                }
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('login_form'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            redirect(base_url('login'));
        }
    }

    function check_email_login($email) {
        $this->load->model('member_model', 'member');
        $query = $this->member->_get(array("member_email" => $email));

        if ($query->num_rows === 1) {
            if ($query->row()->member_active === 'YES') {
                return true;
            } else {
                $this->login_form->set_message('check_email_login', 'Alamat email belum aktif, silahkan aktivasi disini <a href="' . base_url('activasion') . '">aktivasi</a>');
                return false;
            }
        } else {
            $this->login_form->set_message('check_email_login', 'Alamat email belum terdaftar');
            return false;
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */