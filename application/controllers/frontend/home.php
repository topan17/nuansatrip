<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Home extends Frontend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->page_css[] = "{$this->_assets_css}home.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";
        $this->page_css[] = "{$this->_general_assets}flexslider/flexslider.css";

        $this->page_js[] = "{$this->_general_assets}flexslider/jquery.flexslider.js";
        $this->page_js[] = "{$this->_assets_js}home.js";

        parent::index();
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */