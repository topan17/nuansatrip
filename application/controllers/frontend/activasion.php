<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Activasion extends Frontend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->page_css[] = "{$this->_assets_css}activasion.css";
        $this->page_css[] = "{$this->_assets_css}breadcrumb.css";

        $this->page_js[] = "{$this->_assets_js}activasion.js";


        if (!empty($_GET['e']) && !empty($_GET['c'])) {
            $this->load->model('member_model', 'member');
            $where = array(
                'member_email' => $_GET['e'],
                'activasion_code' => $_GET['c']
            );
            $query = $this->member->_get($where);

            if ($query->num_rows === 1) {
                if ($query->row()->member_active === 'YES') {
                    $this->session->set_flashdata('message', 'Akun anda sudah aktif');
                    redirect(current_url());
                } elseif (strtotime($query->row()->activasion_expired) <= strtotime(date('Y-m-d h:i:s'))) {
                    $this->session->set_flashdata('message', '<strong>Expired</strong>, link aktivasi anda sudah expired. Silahkan melakukan aktivasi ulang.');
                    redirect(current_url());
                } else {
                    $this->member->_update(array('member_active' => 'YES'), array('member_email' => $_GET['e']));
                    $this->session->set_flashdata('message', '<strong>Terimakasih</strong>, sekarang akun anda sudah aktif. Silahkan login.');
                    redirect(base_url('login'));
                }
            } else {
                $this->session->set_flashdata('message', 'Link aktivasi anda tidak sesuai');
                redirect(current_url());
            }
        }

        parent::index();
    }

    function request_activasion() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('request_activasion'), "request_activasion");
            if ($this->request_activasion->run()) {
                $this->load->model('member_model', 'member');
                $new_activasion_code = md5(mt_rand() . date('Y-m-d h:i:s') . mt_rand());

                $where = array('member_email' => $this->input->post('email_address'));
                $data = array(
                    'activasion_code' => $new_activasion_code,
                    'activasion_expired' => date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s') . ' + 1 day'))
                );

                $this->member->_update($data, $where);

                $query = $this->member->_get(array("member_email" => $this->input->post('email_address')));

                $this->sendmail('register', $query->row_array(), $this->input->post('email_address'));
                $msg = '<div class="alert alert-success success" role="alert">Silahkan cek email anda, kami telah mengirim link aktivasi ke email ' . $query->row()->member_email . '</div>';
                $json_encode = array(
                    'status' => 'success',
                    'member_email' => $this->input->post('email_address'),
                    'form_success' => $msg
                );
                echo json_encode($json_encode);
                die();
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('request_activasion'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            redirect(base_url('activasion'));
        }
    }

    function check_email_activasion($email) {
        $this->load->model('member_model', 'member');
        $query = $this->member->_get(array("member_email" => $email));

        if ($query->num_rows === 1) {
            if ($query->row()->member_active !== 'YES') {
                return true;
            } else {
                $this->request_activasion->set_message('check_email_activasion', 'Alamat email sudah aktif');
                return false;
            }
        } else {
            $this->request_activasion->set_message('check_email_activasion', 'Alamat email belum terdaftar');
            return false;
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */