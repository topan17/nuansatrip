<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Home extends Frontend {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->view->content('pages/blog/home');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */