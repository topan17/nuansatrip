<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Article extends Frontend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        /* TRACKING VISITOR */
//        $this->load->library('log_cust');
//        $this->log_cust->write_log($this->class_name, "VISIT", current_url());

        $this->page_css[] = "{$this->_general_assets}flexslider/flexslider.css";
        $this->page_js[] = "{$this->_general_assets}flexslider/jquery.flexslider.js";
        $this->page_js[] = "{$this->_assets_js}article.js";
        $this->init_blog();
        $this->view->content('pages/blog/article');
    }

    public function send_comment() {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation", $this->config->item('send_comment'));
            if ($this->form_validation->run()) {
                $this->load->model('comment_model', 'comment');
                $msg = strip_tags($this->input->post('comment_message', true));
                $msg = $this->security->xss_clean($msg);

                $data = array(
                    'comment_email' => $this->input->post('comment_email'),
                    'comment_name' => $this->input->post('comment_name'),
                    'comment_message' => $msg,
                    'comment_created' => date('Y-m-d h:i:s')
                );

                $query = $this->comment->_insert($data);
                if ($query === true) {
                    $msg = '<div class="alert alert-success success" role="alert">Pesan sudah disimpan.</div>';
                    $json_encode = array(
                        'status' => 'success',
                        'form_success' => $msg
                    );
                    echo json_encode($json_encode);
                    die();
                } else {
                    $err = '<div class="alert alert-danger error" role="alert">Database Error</div>';
                    $json_encode = array(
                        'status' => 'error',
                        'form_error' => generate_error_form_validation(array(), $err, TRUE)
                    );
                    echo json_encode($json_encode);
                    die();
                }
            } else {
                $json_encode = array(
                    'status' => 'error',
                    'form_error' => generate_error_form_validation($this->config->item('send_comment'), validation_errors(), FALSE)
                );
                echo json_encode($json_encode);
                die();
            }
        } else {
            redirect(base_url('blog'));
        }
    }

    public function check_article_id($article_id) {
        $this->load->model('article_model', 'article');
        $query = $this->article->_get(array("article_id" => $article_id));

        if ($query->num_rows === 1) {
            if ($query->row()->article_active !== 'YES') {
                $this->form_validation->set_message('check_article_id', 'Artikel belum tayang');
                return false;
            } else {
                return true;
            }
        } else {
            $this->form_validation->set_message('check_article_id', 'Artikel tidak ditemukan');
            return false;
        }
    }

}