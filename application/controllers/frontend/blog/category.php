<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Category extends Frontend {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
       $this->init_blog();
       $this->view->content('pages/blog/category');
    }
}