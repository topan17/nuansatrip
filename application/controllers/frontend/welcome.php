<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/frontend.php';

class Welcome extends Frontend {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->page_css[] = "{$this->_general_assets}plugins/fancybox/source/jquery.fancybox.css";
        $this->page_css[] = "{$this->_assets_css}welcome.css";
        parent::index();
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */