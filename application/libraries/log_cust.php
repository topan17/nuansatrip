<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_cust {
    private $CI;
    protected $_log_path;
    protected $_date_fmt = 'Y-m-d';
    protected $_time_fmt = 'H:i:s';
    protected $_enabled = TRUE;

    public function __construct() {
        $config = & get_config();
        $this->CI = & get_instance();

        $this->_log_path = ($config['log_path_cust'] != '') ? $config['log_path_cust'] : APPPATH.'logs/';

        if(!is_dir($this->_log_path) OR ! is_really_writable($this->_log_path)){
            $this->_enabled = FALSE;
        }

        if($config['environment_status'] == 'development'){
            $this->_enabled = FALSE;
        }
    }

    public function __destruct() {
        unset($this->CI);
    }

    public function write_log($class, $act, $url){
        if($this->_enabled === FALSE){
            return FALSE;
        }

        $filepath = $this->_log_path.'log-'.date('Y-m-d').'.php';
        $message  = '';

        if(!file_exists($filepath)){
            $message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n";
        }

        if(!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)){
            return FALSE;
        }

        $message .= date($this->_date_fmt).';';
        $message .= date($this->_time_fmt).';';
        $message .= $this->CI->input->ip_address().';';
        //$message .= $this->CI->session->userdata ? json_encode($this->CI->session->userdata).';' : 'GUEST;';
        $message .= $class.';';
        $message .= $act.';';
        $message .= $url.';';
        $message .= $this->CI->agent->browser().$this->CI->agent->version().";\n";
        

        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        @chmod($filepath, FILE_WRITE_MODE);
        return TRUE;
    }

    

}

/* End of file log_member.php */
/* Location: ./application/libraries/log_member.php */