<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog_category_model extends CI_Model {

    protected $_table = 'blog_category';

    function __construct() {
        parent::__construct();
    }

    public function _get($where) {
        $query = $this->db->get_where($this->_table, $where);
        return $query;
    }

    public function _insert($data) {
        $query = $this->db->insert($this->_table, $data);
        return $query;
    }

    public function _update($data, $where) {
        $query = $this->db->update($this->_table, $data, $where);
        return $query;
    }

    public function _get_count_article() {
        $query = $this->db->query('SELECT bc.*, COUNT(bc.category_id) AS have_article
            FROM blog_category bc JOIN blog_article ba ON ba.category_id = bc.category_id
            WHERE bc.category_active = "YES" AND ba.article_active = "YES"
            GROUP BY bc.category_id HAVING COUNT(bc.category_id) > 0')->result_array();
        return $query;
    }

}

/* End of file core_model.php */
/* Location: ./application/models/member_model.php */