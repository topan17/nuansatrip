<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_backend_model extends CI_Model {

    protected $_table = 'menu_backend';

    function __construct() {
        parent::__construct();
    }

    public function _get($where) {
        $query = $this->db->get_where($this->_table, $where);
        return $query;
    }

    public function _insert($data) {
        $query = $this->db->insert($this->_table, $data);
        return $query;
    }

    public function _update($data, $where) {
        $query = $this->db->update($this->_table, $data, $where);
        return $query;
    }
    
    public function generate_sidebar_menu($tree = array(), $parent = 0) {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->join('privilege p', 'menu_backend.menu_id = p.menu_backend AND p.privilege_action LIKE "%view%"');
        $this->db->join('user_group ug', 'p.group_id = ug.group_id');
        $this->db->join('user u', 'ug.group_id = u.user_group');
        $this->db->where('menu_backend.menu_parent', $parent);
        $this->db->where('menu_backend.menu_active', 'YES');
        $this->db->where('u.user_group', 1);
        $query = $this->db->get();
        
        $make_array_menu = array();
        foreach($query->result_array() as $i => $item){
            if($item['menu_parent'] == $parent){
                $make_array_menu[$item['menu_id']] = $item;
                $make_array_menu[$item['menu_id']]['submenu'] = $this->generate_sidebar_menu($query->result_array(), $item['menu_id']);
            }
        }

        return $make_array_menu;
    }
    
    public function get_child($parent){
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where('menu_parent', $parent); 
        $count = $this->db->get()->num_rows();
        return $count;
    }
    

}

/* End of file core_model.php */
/* Location: ./application/models/member_model.php */