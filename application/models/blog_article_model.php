<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog_article_model extends CI_Model {

    protected $_table = 'blog_article';

    function __construct() {
        parent::__construct();
    }

    public function _get($where) {
        $query = $this->db->get_where($this->_table, $where);
        return $query;
    }

    public function _insert($data) {
        $query = $this->db->insert($this->_table, $data);
        return $query;
    }

    public function _update($data, $where) {
        $query = $this->db->update($this->_table, $data, $where);
        return $query;
    }
}

/* End of file core_model.php */
/* Location: ./application/models/member_model.php */