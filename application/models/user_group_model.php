<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_group_model extends CI_Model {

    protected $_table = 'user_group';

    function __construct() {
        parent::__construct();
    }

    public function _get($where = array()) {
        $query = $this->db->get_where($this->_table, $where);
        return $query;
    }

    public function _insert($data = array()) {
        $query = $this->db->insert($this->_table, $data);
        return $query;
    }

    public function _update($data = array(), $where = array()) {
        $query = $this->db->update($this->_table, $data, $where);
        return $query;
    }
    
    public function _delete($where = array()) {
        $this->db->delete($this->_table, $where);
    }
    
    public function _generate_grid_table($per_page){
        $this->db->select("*");
        $this->db->from($this->_table);
        
        if (empty($_GET['per_page'])):
            $this->db->limit($per_page, 0);
        else:
            if ($_GET['per_page'] > 0) {
                $this->db->limit($per_page, $_GET['per_page']);
            } else {
                $this->db->limit($per_page, 0);
            }
        endif;
        
        return $this->db->get();
    }
    
    public function _count_table(){
        $this->db->select("*");
        $this->db->from($this->_table);
        
        return $this->db->get();
    }
}

/* End of file core_model.php */
/* Location: ./application/models/member_model.php */