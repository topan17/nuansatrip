<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Privilege_model extends CI_Model {

    protected $_table = 'privilege';

    function __construct() {
        parent::__construct();
    }

    public function _get($where) {
        $query = $this->db->get_where($this->_table, $where);
        return $query;
    }

    public function _insert($data) {
        $query = $this->db->insert($this->_table, $data);
        return $query;
    }

    public function _update($data, $where) {
        $query = $this->db->update($this->_table, $data, $where);
        return $query;
    }

    public function generate_privilege_data($group_id, $tree = array(), $parent = 0) {
        $this->db->select('*');
        $this->db->from('menu_backend mb');
        $this->db->join('privilege p', 'mb.menu_id = p.menu_backend AND p.group_id = ' . $group_id, 'left');
        $this->db->join('user_group ug', 'p.group_id = ug.group_id AND ug.group_id = ' . $group_id, 'left');
        $query = $this->db->get();

        $make_array_menu = array();
        foreach ($query->result_array() as $i => $item) {
            if ($item['menu_parent'] == $parent) {
                $make_array_menu[$item['menu_id']] = $item;
                $make_array_menu[$item['menu_id']]['submenu'] = $this->generate_privilege_data($group_id, $query->result_array(), $item['menu_id']);
                if (preg_match('/view/i', $item['menu_act'])) {
                    $make_array_menu[$item['menu_id']]['act_view'] = preg_match('/view/i', $item['privilege_action']) ? 'YES' : 'NO';
                }

                if (preg_match('/add/i', $item['menu_act'])) {
                    $make_array_menu[$item['menu_id']]['act_add'] = preg_match('/add/i', $item['privilege_action']) ? 'YES' : 'NO';
                }

                if (preg_match('/edit/i', $item['menu_act'])) {
                    $make_array_menu[$item['menu_id']]['act_edit'] = preg_match('/edit/i', $item['privilege_action']) ? 'YES' : 'NO';
                }

                if (preg_match('/delete/i', $item['menu_act'])) {
                    $make_array_menu[$item['menu_id']]['act_delete'] = preg_match('/delete/i', $item['privilege_action']) ? 'YES' : 'NO';
                }

                if (preg_match('/download/i', $item['menu_act'])) {
                    $make_array_menu[$item['menu_id']]['act_download'] = preg_match('/download/i', $item['privilege_action']) ? 'YES' : 'NO';
                }
            }
        }

        return $make_array_menu;
    }

    public function generate_data_action($controller_name) {
        $this->db->select('p.privilege_action');
        $this->db->from('menu_backend mb');
        $this->db->join('privilege p', 'mb.menu_id = p.menu_backend');
        $this->db->join('user_group ug', 'p.group_id = ug.group_id');
        $this->db->where('mb.menu_controller', $controller_name);
        $this->db->where('ug.group_id', "1");
        $query = $this->db->get();
        return $query;
    }

}

/* End of file core_model.php */
/* Location: ./application/models/member_model.php */