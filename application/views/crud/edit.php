<?php foreach ($columns as $index_field => $field): ?>
    <?php if ($field['db'] == $primary_key): ?>
        <?php echo $field['form']; ?>
    <?php else: ?>
        <div class="form-group" for="<?php echo $field['db']; ?>">
            <label for="<?php echo $field['db']; ?>" class="col-sm-2 control-label"><?php echo ucwords(preg_replace("/_/", " ", $field['db'])); ?></label>
            <div class="col-sm-10 controls">
                <?php echo $field['form']; ?>
            </div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>


<?php
//echo '<pre>';
//print_r($row);
//print_r($columns);
//echo '</pre>';
?>