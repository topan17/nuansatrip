<div class="modal fade" id="modal-crud">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <form action="#" name="crud-form" id="crud-form" class="form-horizontal" role="form">
                <div class="modal-body">
                    <p class="text-center loading-data-animation"><img src="<?php echo "{$_assets}img/small-loading.gif"; ?>">&nbsp;Loading...</p>
                    <div id="input-form-content"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button name="submit" type="submit" data-loading-text="Loading..." class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->