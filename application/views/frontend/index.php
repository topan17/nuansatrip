<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $pages_meta['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo $pages_meta['desc']; ?>" />
        <meta name="keywords" content="<?php echo $pages_meta['keywords']; ?>" />
        <?php $this->view->load("font"); ?>
        <?php $this->view->load("assets_css"); ?>
        <?php $this->view->load("global_js_var.php"); ?>
    </head>
    <body>
        <?php $this->view->load("header"); ?>
        <?php $this->view->load($_pages); ?>
        <?php $this->view->load("footer"); ?>
        <?php $this->view->load("chaser"); ?>
        <?php $this->view->load("popup"); ?>
        <?php $this->view->load("assets_js"); ?>

        <?php
//            echo '<pre>';
//            print_r($this->session->userdata);
//            echo '</pre>';
        ?>
    </body>
</html>