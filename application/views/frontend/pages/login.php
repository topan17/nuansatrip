<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">

        <?php if($this->session->flashdata('message')): ?>
        <div class="login-box col-md-offset-3 col-md-6">
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="login-box col-md-offset-3 col-md-6">
            <h2 class="reply-title">Login</h2>
            <div class="login-form-box">
                <form id="form-login">
                    <div class="form-group">
                        <label>Alamat Email</label>
                        <input class="input-text full-width" type="text" name="login_email" placeholde="Alamat email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="input-text full-width" type="password" name="login_password" placeholde="Password">
                    </div>
                    <button class="btn-large full-width" type="submit">MASUK</button>
                </form>
            </div>
        </div>
    </div>
</div>