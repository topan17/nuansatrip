<?php //$this->view->load("breadcrumb");  ?>
<div id="content">
    <div class="container">
        <div class="clearfix content-top">
            <div class="banner-box col-md-8" style="padding: 0;">
                <div class="flexslider banner-box-slider" style="padding: 3px;">
                    <ul class="slides">
                        <li>
                            <a href=""><img class="img-responsive" src="<?php echo "{$_assets}img/bg3-.jpg"; ?>" /><div class="flex-caption">Some captions for lorem ipsum bubur sumsum bubur kacang ijo pisang ijo campur gado-gado dan ketoprak gurih-gurih nyoi</div></a>
                        </li>
                        <li>
                            <a href=""><img class="img-responsive" src="<?php echo "{$_assets}img/bg3-.jpg"; ?>" /><div class="flex-caption">Some captions for lorem ipsum bubur sumsum bubur kacang ijo pisang ijo campur gado-gado dan ketoprak gurih-gurih nyoi</div></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-container-box col-md-4" style="padding: 0;">
                <div style="background-color: #fff; padding: 3px 5px 10px 10px;">
                    <ul class="tabs full-width">
                        <li class="active">
                            <a data-toggle="tab" href="#popular-posts" data-toggle="tab">Popular</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#new-posts" data-toggle="tab">New</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#recent-posts" data-toggle="tab">Recent Post</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="popular-posts" class="tab-pane fade in active">
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Half-Day Island Tour</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">210</span> Views
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Jalan - jalan dengan singkong rebus pake telor setengah mateng</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">210</span> Views
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Pecahkan saja gelasnya biar beli baru</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">210</span> Views
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Lorem ipsum bubur sumsum</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">210</span> Views
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Lorem ipsum bubur sumsum</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">210</span> Views
                                    </label>
                                </div>
                            </article>
                        </div>
                        <div id="new-posts" class="tab-pane fade">
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Half-Day Island Tour</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Jalan - jalan dengan singkong rebus</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Pecahkan saja gelasnya biar beli baru</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Lorem ipsum bubur sumsum</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                        </div>
                        <div id="recent-posts" class="tab-pane fade">
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Half-Day Island Tour</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Jalan - jalan dengan singkong rebus</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Pecahkan saja gelasnya biar beli baru</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                            <article class="box">
                                <div class="details">
                                    <h5 class="box-title">
                                        <a href="#">Lorem ipsum bubur sumsum</a>
                                    </h5>
                                    <label class="info">
                                        <span class="viewer">10 jan 2014</span>
                                    </label>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--RECENT ARTICLE-->
        <h2>Recent Article</h2>
        <div class="block image-carousel flexslider box-slider2">
            <ul class="slides image-box listing-style">
                <li>
                <article class="box">
                    <figure>
                        <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                            <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                        </a>
                    </figure>
                    <div class="details">
                        <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                        <div class="box-title-overflow">
                            <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                        </div>
                        <label class="price-wrapper">
                            IDR <span class="price-per-unit">100.000</span>
                        </label>
                    </div>
                </article>
                </li>
                <li>
                <article class="box">
                    <figure>
                        <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                            <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                        </a>
                    </figure>
                    <div class="details">
                        <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                        <div class="box-title-overflow">
                            <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                        </div>
                        <label class="price-wrapper">
                            IDR <span class="price-per-unit">100.000</span>
                        </label>
                    </div>
                </article>
                </li>
                <li>
                <article class="box">
                    <figure>
                        <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                            <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                        </a>
                    </figure>
                    <div class="details">
                        <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                        <div class="box-title-overflow">
                            <h4 class="box-title">Dinner at Hotel Mercure Resto bersama pacar tercinta enak sekali</h4>
                        </div>
                        <label class="price-wrapper">
                            IDR <span class="price-per-unit">100.000</span>
                        </label>
                    </div>
                </article>
                </li>
                <li>
                <article class="box">
                    <figure>
                        <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                            <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                        </a>
                    </figure>
                    <div class="details">
                        <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                        <div class="box-title-overflow">
                            <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                        </div>
                        <label class="price-wrapper">
                            IDR <span class="price-per-unit">100.000</span>
                        </label>
                    </div>
                </article>
                </li>
                <li>
                <article class="box">
                    <figure>
                        <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                            <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                        </a>
                    </figure>
                    <div class="details">
                        <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                        <div class="box-title-overflow">
                            <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                        </div>
                        <label class="price-wrapper">
                            IDR <span class="price-per-unit">100.000</span>
                        </label>
                    </div>
                </article>
                </li>
            </ul>
        </div>

        <!--RECOMMENDED PRODUCT-->
        <!--        <h2>Recommended Products</h2>
                <div class="block image-carousel flexslider box-slider1">
                    <ul class="slides image-box listing-style">
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                                        <img src="<?php echo "{$_assets}img/product1.jpg" ?>" alt="" width="270" height="160" />
                                    </a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                                        <img src="<?php echo "{$_assets}img/product2.jpg" ?>" alt="" width="270" height="160" />
                                    </a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery"><img src="http://www.soaptheme.com/html/travelo/images/shortcodes/gallery-popup/3.jpg" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery"><img src="http://www.soaptheme.com/html/travelo/images/shortcodes/gallery-popup/4.jpg" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery"><img src="http://www.soaptheme.com/html/travelo/images/shortcodes/gallery-popup/5.jpg" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                    </ul>
                </div>-->

        <!--RECOMMENDED PRODUCT-->
        <!--        <h2>New Products</h2>
                <div class="block image-carousel flexslider box-slider3">
                    <ul class="slides image-box listing-style">
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                                        <img src="<?php echo "{$_assets}img/product1.jpg" ?>" alt="" width="270" height="160" />
                                    </a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                                        <img src="<?php echo "{$_assets}img/product2.jpg" ?>" alt="" width="270" height="160" />
                                    </a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery"><img src="http://www.soaptheme.com/html/travelo/images/shortcodes/gallery-popup/3.jpg" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery"><img src="http://www.soaptheme.com/html/travelo/images/shortcodes/gallery-popup/4.jpg" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure>
                                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery"><img src="http://www.soaptheme.com/html/travelo/images/shortcodes/gallery-popup/5.jpg" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">buy</a>
                                    <div class="box-title-overflow">
                                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                                    </div>
                                    <label class="price-wrapper">
                                        IDR <span class="price-per-unit">100.000</span>
                                    </label>
                                </div>
                            </article>
                        </li>
                    </ul>
                </div>-->
    </div>
</div>