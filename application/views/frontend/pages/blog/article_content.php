<div class="post">
    <figure class="image-container">
        <a href="#">
            <img class="img-responsive" alt="" src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png">
        </a>
    </figure>
    <div class="details">
        <h1 class="entry-title">Standard single image post</h1>
        <div class="post-content">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a neque a tortor tempor in porta sem vulputate. Donec varius felis fermentum nis type specimen book. It has survived not only five centuries.Phasellus vehicula justo eget diam posuere sollicitudin eu tincidunt nulla. Curabitur eleifend tempor magna, in scelerisque urna placerat vel. Phasellus eget sem id justo consequat egestas quis facilisis metus.Phasellus vehicula justo eget diam posuere sollicitudin eu tincidunt nulla. Curabitur eleifend tempor magna, in scelerisque urna placerat vel. Phasellus eget sem id justo consequat egestas quis facilisis metus.</p>
            <blockquote class="style2">
                <p>Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit Massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet egeat. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit sodales volutpat sapien varius vel. </p>
            </blockquote>
            <p>Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan. Nunc a vulputate lectus. Vestibulum eleifend nisl sed massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet eget. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit. Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan. Nunc a vulputate lectus. Vestibulum eleifend nisl sed massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet eget. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit.</p>
            <p>Massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet eget. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit.</p>
            <img class="img-responsive" alt="" src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" style="display: block; margin: 0 auto;" />
            <p style="text-align: center; font-style: italic;">auk ah nih gambar apaan</p>
            <img class="img-responsive" src="<?php echo "{$_assets}img/testaja.jpg"; ?>" style="display: block; margin: 0 auto;" />
            <p>Welcome drink diberikan kepada saya setelah sampai di Hotel Centro. Es Lemon tea yang segar memang pas banget untuk siang hari negara tropis seperti Filipina, sembari menunggu kamar yang sedang dipersiapkan. Memang saya datang terlalu awal dari yang dijadwalkan, jadi kamar saya belum siap. Mereka mempersilahkan menunggu di lobi Hotel Centro yang nyaman buat menunggu.</p>
        </div>
        <div class="post-meta">
            <div class="entry-date">
                <label class="date">29</label>
                <label class="month">Aug</label>
            </div>
            <div class="entry-author fn">
                <i class="fa fa-user"></i>Posted By: <a class="author" href="#">Jessica Browen</a>
            </div>
            <div class="entry-action">
                <a class="button entry-comment btn-small" href="#">
                    <i class="fa fa-comments"></i><span>30 Comments</span>
                </a>
                <a class="button btn-small" href="#">
                    <i class="fa fa-heart"></i><span>22 Likes</span>
                </a>
                <span class="entry-tags">
                    <i class="fa fa-tags"></i><span><a href="#">Adventure</a>, <a href="#">Romance</a></span>
                </span>
            </div>
        </div>
    </div>
    <div class="single-navigation">
        <div class="row">
            <div class="col-xs-6">
                <a class="button col-xs-12" href="#">
                    <i class="fa fa-long-arrow-left"></i> <span>Previous Post</span>
                </a>
            </div>
            <div class="col-xs-6">
                <a class="button col-xs-12" href="#">
                    <span>Next Post</span> <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <h2>Artikel Lainnya</h2>
    <div class="block image-carousel flexslider box-slider2">
        <ul class="slides image-box listing-style">
            <li>
            <article class="box">
                <figure>
                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                        <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                    </a>
                </figure>
                <div class="details">
                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                    <div class="box-title-overflow">
                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                    </div>
                    <label class="price-wrapper">
                        IDR <span class="price-per-unit">100.000</span>
                    </label>
                </div>
            </article>
            </li>
            <li>
            <article class="box">
                <figure>
                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                        <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                    </a>
                </figure>
                <div class="details">
                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                    <div class="box-title-overflow">
                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                    </div>
                    <label class="price-wrapper">
                        IDR <span class="price-per-unit">100.000</span>
                    </label>
                </div>
            </article>
            </li>
            <li>
            <article class="box">
                <figure>
                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                        <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                    </a>
                </figure>
                <div class="details">
                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                    <div class="box-title-overflow">
                        <h4 class="box-title">Dinner at Hotel Mercure Resto bersama pacar tercinta enak sekali</h4>
                    </div>
                    <label class="price-wrapper">
                        IDR <span class="price-per-unit">100.000</span>
                    </label>
                </div>
            </article>
            </li>
            <li>
            <article class="box">
                <figure>
                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                        <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                    </a>
                </figure>
                <div class="details">
                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                    <div class="box-title-overflow">
                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                    </div>
                    <label class="price-wrapper">
                        IDR <span class="price-per-unit">100.000</span>
                    </label>
                </div>
            </article>
            </li>
            <li>
            <article class="box">
                <figure>
                    <a href="ajax/slideshow-popup.html" class="hover-effect popup-gallery">
                        <img src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png" alt="" class="img-responsive" />
                    </a>
                </figure>
                <div class="details">
                    <a title="View all" href="hotel-detailed.html" class="pull-right button uppercase">read</a>
                    <div class="box-title-overflow">
                        <h4 class="box-title">Gran Canaria Apa klkokw jlk wqjke kqwjekjqkw iuio</h4>
                    </div>
                    <label class="price-wrapper">
                        IDR <span class="price-per-unit">100.000</span>
                    </label>
                </div>
            </article>
            </li>
        </ul>
    </div>

    <div class="comments-container">
        <h2>3 Komentar</h2>
        <ul class="comment-list">
            <li class="comment depth-1">
                <div class="the-comment">
<!--                    <div class="avatar">
                        <img width="72" height="72" alt="" src="http://www.soaptheme.com/html/travelo/images/shortcodes/team/david.png">
                    </div>-->
                    <div class="comment-box">
                        <div class="comment-author">
                            <h4 class="box-title">David Jhon <small>Nov, 12, 2013</small></h4>
                        </div>
                        <div class="comment-text">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took.</p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="comment depth-1">
                <div class="the-comment">
<!--                    <div class="avatar">
                        <img width="72" height="72" alt="" src="http://www.soaptheme.com/html/travelo/images/shortcodes/team/david.png">
                    </div>-->
                    <div class="comment-box">
                        <div class="comment-author">
                            <h4 class="box-title">David Jhon <small>Nov, 12, 2013</small></h4>
                        </div>
                        <div class="comment-text">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took.</p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="post-comment">
        <h2 class="reply-title">Kirim komentar anda</h2>
        <div class="comment-form-box">
            <form id="form-comment-article">
                <div class="form-group row">
                    <div class="col-xs-6">
                        <label>Nama lengkap</label>
                        <input class="input-text full-width" name="comment_name" type="text">
                    </div>
                    <div class="col-xs-6">
                        <label>Alamat email</label>
                        <input class="input-text full-width" name="comment_email" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label>Komentar anda</label>
                    <textarea class="input-text full-width" name="comment_message" placeholder="tulis komentar anda disini" rows="6"></textarea>
                </div>
                <div id="recaptcha_div" style="display:none" class="col-md-offset-4 col-md-4">
                    <?php echo $this->view->load('recaptcha'); ?>
                </div>
                <button class="btn-large full-width" type="submit">KIRIM KOMENTAR</button>
                <input class="input-text full-width" name="article_id" value="<?php echo '100'; ?>" type="hidden">
            </form>
        </div>
    </div>
</div>