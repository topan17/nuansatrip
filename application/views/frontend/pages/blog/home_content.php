<div class="page">
    <div class="post-content">
        <div class="post">
            <div class="post-content-wrapper">
                <figure class="image-container text-center">
                    <a class="hover-effect" href="#">
                        <img alt="" class="img-responsive" src="http://www.soaptheme.com/html/travelo/images/blog/post/image/1.png">
                    </a>
                </figure>
                <div class="details">
                    <h2 class="entry-title">
                        <a href="pages-blog-read.html">Standard single image post</a>
                    </h2>
                    <div class="excerpt-container">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a neque a tortor tempor in porta sem vulputate. Donec varius felis fermentum nis type specimen book. It has survived not only five centuries.</p>
                    </div>
                    <div class="post-meta">
                        <div class="entry-date">
                            <label class="date">29</label>
                            <label class="month">Aug</label>
                        </div>
                        <div class="entry-author fn">
                            <i class="fa fa-user"></i>Posted By : <a class="author" href="#">Jessica Browen</a>
                        </div>
                        <div class="entry-action">
                            <a class="button entry-comment btn-small" href="#">
                                <i class="fa fa-comments"></i><span>30 Comments</span>
                            </a>
                            <a class="button btn-small" href="#">
                                <i class="fa fa-heart"></i><span>22 Likes</span>
                            </a>
                            <span class="entry-tags">
                                <i class="fa fa-tags"></i><span><a href="#">Adventure</a>, <a href="#">Romance</a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="post">
            <div class="post-content-wrapper">
                <div class="details">
                    <h2 class="entry-title without-img">
                        <a href="pages-blog-read.html">Standard single image post</a>
                    </h2>
                    <div class="excerpt-container">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a neque a tortor tempor in porta sem vulputate. Donec varius felis fermentum nis type specimen book. It has survived not only five centuries.</p>
                    </div>
                    <div class="post-meta">
                        <div class="entry-date without-img">
                            <label class="date">29</label>
                            <label class="month">Aug</label>
                        </div>
                        <div class="entry-author fn">
                            <i class="fa fa-user"></i>Posted By : <a class="author" href="#">Jessica Browen</a>
                        </div>
                        <div class="entry-action">
                            <a class="button entry-comment btn-small" href="#">
                                <i class="fa fa-comments"></i><span>30 Comments</span>
                            </a>
                            <a class="button btn-small" href="#">
                                <i class="fa fa-heart"></i><span>22 Likes</span>
                            </a>
                            <span class="entry-tags">
                                <i class="fa fa-tags"></i><span><a href="#">Adventure</a>, <a href="#">Romance</a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>