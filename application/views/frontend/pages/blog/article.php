<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9" style="min-height: 750px;">
                <?php $this->view->load("pages/blog/article_content"); ?>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                <?php $this->view->load("pages/blog/sidebar"); ?>
            </div>
        </div>
    </div>
</div>