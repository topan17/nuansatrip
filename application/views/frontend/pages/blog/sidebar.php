<div class="search-box">
    <h5 class="box-title">Search</h5>
    <div class="with-icon full-width">
        <input class="input-text full-width" type="text" placeholder="title or category">
        <button class="icon green-bg color-white">
            <i class="fa fa-search"></i>
        </button>
    </div>
</div>
<div class="category-box">
    <h5 class="box-title">All Categories</h5>
    <ul class="categories-filter">
        <li><a href="<?php echo base_url('blog/all-category'); ?>">ALL<small>(<?php echo $count_article; ?>)</small></a></li>
        <?php foreach($category_blog as $row => $list): ?>
        <li><a href="<?php echo base_url('blog/'.$list['category_url']); ?>"><?php echo $list['category_name']; ?><small>(<?php echo $list['have_article']; ?>)</small></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="tab-container-box">
    <ul class="tabs full-width">
        <li class="active">
            <a data-toggle="tab" href="#popular-posts" data-toggle="tab">Popular</a>
        </li>
        <li>
            <a data-toggle="tab" href="#new-posts" data-toggle="tab">New</a>
        </li>
        <li>
            <a data-toggle="tab" href="#recent-posts" data-toggle="tab">Recent Post</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="popular-posts" class="tab-pane fade in active">
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Half-Day Island Tour</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">210</span> Views
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Jalan - jalan dengan singkong rebus</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">210</span> Views
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Pecahkan saja gelasnya biar beli baru</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">210</span> Views
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Lorem ipsum bubur sumsum</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">210</span> Views
                    </label>
                </div>
            </article>
        </div>
        <div id="new-posts" class="tab-pane fade">
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Half-Day Island Tour</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Jalan - jalan dengan singkong rebus</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Pecahkan saja gelasnya biar beli baru</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Lorem ipsum bubur sumsum</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
        </div>
        <div id="recent-posts" class="tab-pane fade">
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Half-Day Island Tour</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Jalan - jalan dengan singkong rebus</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Pecahkan saja gelasnya biar beli baru</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
            <article class="box">
                <div class="details">
                    <h5 class="box-title">
                        <a href="#">Lorem ipsum bubur sumsum</a>
                    </h5>
                    <label class="info">
                        <span class="viewer">10 jan 2014</span>
                    </label>
                </div>
            </article>
        </div>
    </div>
</div>