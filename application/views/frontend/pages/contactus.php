<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="sidebar col-sm-4 col-md-3">
                <div class="contacus-sidebar">
                    <h5 class="box-title">Contact us</h5>
                    <ul class="contact-address">
                        <li>
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-square-o fa-stack-2x"></i>
                                <i class="fa fa-home fa-stack-1x"></i>
                            </span>
                            <h5 class="title">Address</h5>
                            <p>Jl. Hijau Lestari B4/7 KOMP. PONDOK HIJAU CPUTAT TANGERANG SELATAN BANTEN 15419</p>
                        </li>
                        <li>
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-square-o fa-stack-2x"></i>
                                <i class="fa fa-phone fa-stack-1x"></i>
                            </span>
                            <h5 class="title">Phone</h5>
                            <p>Mobile: <strong>0812-86076-999</strong></p>
                        </li>
                        <li>
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-square-o fa-stack-2x"></i>
                                <i class="fa fa-envelope fa-stack-1x"></i>
                            </span>
                            <h5 class="title">Email</h5>
                            <p>nuansatrip@gmail.com</p>
                            <p>nuansatrip@yahoo.com</p>
                        </li>
                    </ul>
                    <ul class="social-icons full-width">
                        <li>
                            <a title="" data-toggle="tooltip" href="<?php echo $this->config->item('twitter_account'); ?>" data-original-title="Twitter" target="_blank">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="main" class="col-sm-8 col-md-9" style="min-height: 750px;">
                <div class="post-comment">
                    <div class="contactus-form-box">
                        <form id="form-contactus">
                            <div class="form-group row">
                                <div class="col-sm-6 for-contactus_name">
                                    <label>Nama lengkap</label>
                                    <input class="input-text full-width" name="contactus_name" type="text">
                                </div>
                                <div class="col-sm-6 for-contactus_email">
                                    <label>Alamat email</label>
                                    <input class="input-text full-width" name="contactus_email" type="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Pesan</label>
                                <textarea class="input-text full-width" name="contactus_message" placeholder="Tulis pesan disini" rows="6"></textarea>
                            </div>
                            <div id="recaptcha_div" style="display:none" class="col-md-offset-4 col-md-4">
                                <?php echo $this->view->load('recaptcha'); ?>
                            </div>
                            <button class="btn-large full-width" type="submit">KIRIM PESAN</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>