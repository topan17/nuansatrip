<div class="container clearfix profile-box">
    <!-- BEGIN ABOUT INFO -->   
    <div class="row margin-bottom-30">
        <!-- BEGIN INFO BLOCK -->               
        <div class="col-md-7 space-mobile about-info">
            <h2>Vero eos et accusamus</h2>
            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.</p> 
            <p>Idest laborum et dolorum fuga. Et harum quidem rerum et quas molestias excepturi sint occaecati facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus.</p>
            <!-- BEGIN LISTS -->
            <div class="row front-lists-v1">
                <div class="col-md-6">
                    <ul class="list-unstyled margin-bottom-20">
                        <li><span class="glyphicon glyphicon-ok"></span> Officia deserunt molliti</li>
                        <li><span class="glyphicon glyphicon-ok"></span> Consectetur adipiscing </li>
                        <li><span class="glyphicon glyphicon-ok"></span> Deserunt fpicia</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-unstyled">
                        <li><span class="glyphicon glyphicon-ok"></span> Officia deserunt molliti</li>
                        <li><span class="glyphicon glyphicon-ok"></span> Consectetur adipiscing </li>
                        <li><span class="glyphicon glyphicon-ok"></span> Deserunt fpicia</li>
                    </ul>
                </div>
            </div>
            <!-- END LISTS -->
        </div>
        <!-- END INFO BLOCK -->   

        <!-- BEGIN CAROUSEL -->            
        <div class="col-md-5 front-carousel">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="<?php echo "{$_assets}"; ?>img/img2-medium.jpg" alt="">
                        <div class="carousel-caption">
                            <p>Excepturi sint occaecati cupiditate non provident</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo "{$_assets}"; ?>img/img1-medium.jpg" alt="">
                        <div class="carousel-caption">
                            <p>Ducimus qui blanditiis praesentium voluptatum</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo "{$_assets}"; ?>img/img2-medium.jpg" alt="">
                        <div class="carousel-caption">
                            <p>Ut non libero consectetur adipiscing elit magna</p>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>     
        </div>
        <!-- END CAROUSEL -->             
    </div>
    <!-- END ABOUT INFO -->   

    <div class="row tree-boxes">
        <div class="col-md-4 col-sm-4 clearfix">
            <div class="profile-box-heading">
                <em><span class="glyphicon glyphicon-question-sign color-blue"></span></em>
                <span class="profile-title">Apa itu Nano Spray ??</span>
            </div>
            <div class="profile-box-desc">
                Nano Spray adalah alat yang merubah ukuran air menjadi partikel nano, jadi nano itu sendiri adalah ukuran yang lebih kecil daripada mikro. Untuk menggambarkan seberapa kecilnya parktikel nano itu, sehelai rambut anda dibelah 150 kali, itu lah yang dihasilkan oleh Nano Spray 2 ini.
                Karena begitu kecilnya air yang dihasilkan oleh Nano Spray 2 ini, sehingga membuat nutrisi yang di dalam air (O2/Oksigen) ini meresap ke pori-pori kulit...
            </div>
            <p>
                <button class="btn btn-primary pull-right readmore" data-toggle="modal" data-target="#box-what">Read more <span class="glyphicon glyphicon-chevron-right"></span></button>
            </p>
            <div class="modal fade" id="box-what">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Apa itu Nano Spray</h4>
                        </div>
                        <div class="modal-body" style="line-height: 25px;">
                            <p class="text-center"><img src="<?php echo $_assets; ?>img/nanospray.jpg" alt="nano spray" style="margin-bottom: 5px;"></p>
                            <p>Nano Spray adalah alat yang merubah ukuran air menjadi partikel nano, jadi nano itu sendiri adalah ukuran yang lebih kecil daripada mikro. Untuk menggambarkan seberapa kecilnya parktikel nano itu, sehelai rambut anda dibelah 150 kali, itu lah yang dihasilkan oleh Nano Spray 2 ini.</p>
                            <p>
                                Karena begitu kecilnya air yang dihasilkan oleh Nano Spray 2 ini, sehingga membuat nutrisi yang di dalam air (O2/Oksigen) ini meresap ke pori-pori kulit. Partikel nano mampu meresap hingga ke lapisan kulit ke 3 yaitu lapisan lemak, dimana pada lapisan ini tempat terjadinya penumpukan lemak dan kotoran. 
                                Teknologi pada Nano Spray 2 ini memiliki getaran yang berkecepatan tinggi pada frekuensi 1 Ghz, sehingga mampu memecah partikel air untuk melewati 2000 lubang berukuran 0.1 – 0,3 mikron.
                            </p>
                            <p>
                                Dengan ukuran yang sangat kecil ini membuat patikel nano meresap kedalam pori-pori, bahkan pada kulit wajah yang sedang menggunakan make up namun tidak merusak riasan pada wajah anda.
                                Jika anda berfikir ini sama dengan mencuci muka karena sama-sama dengan menggunakan air, sama sekali tidak sama. Mencuci muka hanya menhilangkan kotoran dipermukaannya saja, sedangkan sumber permasalahan kulit seperti jerawat, flek hitam, komedo, dan lain-lain adanya didalam kulit. Anda perlu partikel nano untuk dapat mengangkat kotoran sehingga kulit anda menjadi sehat dan bersih kembali.
                            </p>
                            <p class="text-center">
                                <iframe width="560" height="315" src="//www.youtube.com/embed/O8WXCMkP-vw" frameborder="0" allowfullscreen></iframe>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <div class="col-md-4 col-sm-4 clearfix">
            <div class="profile-box-heading">
                <em><span class="glyphicon glyphicon-ok-sign color-red"></span></em>
                <span class="profile-title">Manfaat dari Nano Spray</span>
            </div>
            <div class="profile-box-desc">
                <ul>
                    <li>Mencerahkan wajah dan mencegah penuaan dini
                    <li>Melembabkan kulit wajah, termasuk melindungi kulit sensitif
                    <li>Mengecilkan pori-pori kulit wajah
                    <li>Mengencangkan kulit wajah
                    <li>Menghilangkan jerawat dan bekas jerawat
                    <li>Menghilangkan flek atau noda hitam di wajah ...
                </ul>
            </div>
            <p>
                <button class="btn btn-danger pull-right readmore" data-toggle="modal" data-target="#box-usage">Read more <span class="glyphicon glyphicon-chevron-right"></span></button>
            </p>
            <div class="modal fade" id="box-usage">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Manfaat dari Nano Spray</h4>
                        </div>
                        <div class="modal-body" style="line-height: 25px;">
                            <p class="text-center"><img src="<?php echo $_assets; ?>img/hasil-penggunaan-nanospray-2.gif" alt="manfaat dari penggunaan nano spray" style="margin-bottom: 5px;"></p>
                            <ul>
                                <li>Mencerahkan wajah dan mencegah penuaan dini
                                <li>Melembabkan kulit wajah, termasuk melindungi kulit sensitif
                                <li>Mengecilkan pori-pori kulit wajah
                                <li>Mengencangkan kulit wajah
                                <li>Menghilangkan jerawat dan bekas jerawat
                                <li>Menghilangkan flek atau noda hitam di wajah
                                <li>Menyamarkan lingkaran hitam di bawah mata (kantung mata)
                                <li>Membersihkan komedo
                            </ul>
                            <p>
                                <strong class="text-danger">APAKAH ADA EFFEK SAMPING DENGAN MENGGUNAKAN NANO SPRAY ??</strong><br/>
                                <strong class="text-success">Jawabannya TIDAK</strong>, Nano Spray hanya menggunakan bahan alami yaitu Air Oksigen (Oxy, Cleo, Super O2, RO). Oksigen adalah salah satu komponen yang sangat dibutuhkan untuk mendapatkan kulit yang bersih selain nutrisi makanan. Dengan bantuan alat Nano Spray air yang mengandung oksigen tersebut akan terserap sempurna ke dalam kulit dan memberikan nutrisi pada kulit wajah Anda.
                            </p>
                            <p>
                                <strong class="text-info">Alat ini hanya perlu AIR!</strong> Anda tidak perlu obat, cream, apalagi tindakan operasi untuk mempercantik wajah. Anda hanya perlu mengisi Nano Spray dengan air secukupnya dan menyemprotkan air tersebut dengan Nano Spray sesering mungkin ke bagian wajah yang bermasalah. Simple banget bukan? Anda bisa melakukannya di mana saja, sambil nonton televisi, sambil nunggu anak bubaran sekolah, sambil duduk santai dengan keluarga, Nano Spray juga mudah dan praktis dibawa dalam perjalanan
                            </p>
                            <p class="text-center">
                                <iframe width="420" height="315" src="//www.youtube.com/embed/ceZBwpacfcc" frameborder="0" allowfullscreen></iframe>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <div class="col-md-4 col-sm-4 clearfix">
            <div class="profile-box-heading">
                <em><span class="glyphicon glyphicon-info-sign color-green"></span></em>
                <span class="profile-title">Cara Pemakaian</span>
            </div>
            <div class="profile-box-desc">
                Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.
            </div>
            <p>
                <button class="btn btn-success pull-right readmore" data-toggle="modal" data-target="#box-how-to-use">Read more <span class="glyphicon glyphicon-chevron-right"></span></button>
            </p>
            <div class="modal fade" id="box-how-to-use">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Cara Pemakaian</h4>
                        </div>
                        <div class="modal-body" style="line-height: 25px;">
                            <p class="text-center"><iframe width="560" height="315" src="//www.youtube.com/embed/3p1WPjOn-M0" frameborder="0" allowfullscreen></iframe></p>
                            <p class="text-center"><iframe width="560" height="315" src="//www.youtube.com/embed/o3dE5UgOMXI" frameborder="0" allowfullscreen></iframe></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>
    <div class="row two-boxes">
        <div class="col-md-8 col-sm-8 clearfix">
            <div class="profile-box-heading">
                <em><span class="glyphicon glyphicon-comment color-grey radius"></span></em>
                <span class="profile-title">Testimonial</span>
                <!-- Carousel nav -->
                <div class="pull-right">
                    <a class="left-btn" href="#myCarousel1" data-slide="prev"><span class="glyphicon glyphicon-chevron-left testimonials-nav"></span></a>
                    <a class="right-btn" href="#myCarousel1" data-slide="next"><span class="glyphicon glyphicon-chevron-right testimonials-nav"></span></a>
                </div>
            </div>
            <!-- BEGIN TESTIMONIALS -->
            <div class="testimonials-v1">
                <div id="myCarousel1" class="">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <span class="testimonials-slide">Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met consectetur adipisicing sit amet do eiusmod dolore.</span>
                            <div class="carousel-info">
                                <div class="pull-left">
                                    <span class="testimonials-name">Lina Doe</span>
                                    <span class="testimonials-post">Commercial Director</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <span class="testimonials-slide">Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</span>
                            <div class="carousel-info">
                                <div class="pull-left">
                                    <span class="testimonials-name">Lina Doe</span>
                                    <span class="testimonials-post">Commercial Director</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <span class="testimonials-slide">Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</span>
                            <div class="carousel-info">
                                <div class="pull-left">
                                    <span class="testimonials-name">Lina Doe</span>
                                    <span class="testimonials-post">Commercial Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TESTIMONIALS -->  
        </div>
        <div class="col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1 clearfix">
            <div class="profile-box-heading">
                <em><span class="glyphicon glyphicon-earphone color-purple radius"></span></em>
                <span class="profile-title">Hubungi Kami</span>
            </div>
            <!-- BEGIN OUR TEAM -->
            <div class="front-team">
                <ul class="list-unstyled">
                    <li class="space-mobile">
                        <div class="thumbnail">
                            <img src="assets/img/people/img1-large.jpg" alt="">
                            <h3>
                                <a>Lina Doe</a> 
                                <small>Chief Executive Officer / CEO</small>
                            </h3>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p>
                            <ul class="social-icons social-icons-color">
                                <li><a href="#" data-original-title="Facebook" class="facebook"></a></li>
                                <li><a href="#" data-original-title="Twitter" class="twitter"></a></li>
                                <li><a href="#" data-original-title="Goole Plus" class="googleplus"></a></li>
                                <li><a href="#" data-original-title="Linkedin" class="linkedin"></a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>