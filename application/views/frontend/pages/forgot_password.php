<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">
        <div class="forgotpass-box col-md-offset-3 col-md-6">
            <h2 class="reply-title">Lupa password</h2>
            <div class="forgotpass-form-box">
                <form id="form-forgotpass">
                    <div class="form-group">
                        <label>Alamat Email</label>
                        <input class="input-text full-width" type="text" name="email_address" placeholde="Alamat email">
                    </div>
                    <button class="btn-large full-width" type="submit">KIRIM</button>
                </form>
            </div>
        </div>
    </div>
</div>