<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">
        <div class="large-block">
            <h1 class="title">We’re truely dedicated to make your travel experience as much simple and fun as possible!</h1>
            <p>Bringing you a modern, comfortable, and connected travel experience is one of our highest priorities and that’s why we continuously try to improve your experience when you book anything with us. We really appreciate and welcome any of suggstions you might have for us, so feel free drop us line anytime.</p>
        </div>
        <div class="large-block mobile-center clearfix">
            <h1>Nuansa Trip Team</h1>
            <div class="image-box clearfix">
                <div class="col-sm-6 col-md-3">
                    <article class="box">
                        <figure>
                            <a href="#">
                                <img class="img-responsive" alt="" src="http://www.soaptheme.com/html/travelo/images/shortcodes/team/jessica.png">
                            </a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title">
                                <a href="#">Jessica Brown<small>Chief Executive</small></a>
                            </h4>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-md-3">
                    <article class="box">
                        <figure>
                            <a href="#">
                                <img class="img-responsive" alt="" src="http://www.soaptheme.com/html/travelo/images/shortcodes/team/jessica.png">
                            </a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title">
                                <a href="#">Jessica Brown<small>Chief Executive</small></a>
                            </h4>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-md-3">
                    <article class="box">
                        <figure>
                            <a href="#">
                                <img class="img-responsive" alt="" src="http://www.soaptheme.com/html/travelo/images/shortcodes/team/jessica.png">
                            </a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title">
                                <a href="#">Jessica Brown<small>Chief Executive</small></a>
                            </h4>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-md-3">
                    <article class="box">
                        <figure>
                            <a href="#">
                                <img class="img-responsive" alt="" src="http://www.soaptheme.com/html/travelo/images/shortcodes/team/jessica.png">
                            </a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title">
                                <a href="#">Jessica Brown<small>Chief Executive</small></a>
                            </h4>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>