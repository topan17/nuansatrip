<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">
        <div class="register-box col-md-offset-3 col-md-6">
            <h2 class="reply-title">Daftar menjadi member</h2>
            <div class="register-form-box">
                <form id="form-register">
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input class="input-text full-width" name="member_name" type="text">
                    </div>
                    <div class="form-group">
                        <label>Alamat Email</label>
                        <input class="input-text full-width" name="member_email" type="text">
                    </div>
                    <div class="form-group">
                        <label>Kata Sandi</label>
                        <input class="input-text full-width" name="member_password" type="password">
                    </div>
                    <div class="form-group">
                        <label>Ulang kata Sandi</label>
                        <input class="input-text full-width" name="member_password_check" type="password">
                    </div>
                    <button class="btn-large full-width" type="submit">DAFTAR</button>
                </form>
            </div>
        </div>
    </div>
</div>