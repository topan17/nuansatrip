<?php $this->view->load("breadcrumb"); ?>
<div id="content">
    <div class="container">
        <?php if($this->session->flashdata('message')): ?>
        <div class="login-box col-md-offset-3 col-md-6">
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <div class="activasion-box col-md-offset-3 col-md-6">
            <h2 class="reply-title">Kirim Ulang Email Aktivasi</h2>
            <div class="activasion-form-box">
                <form id="form-activasion">
                    <div class="form-group">
                        <label>Alamat Email</label>
                        <input class="input-text full-width" type="text" name="email_address" placeholde="Alamat email">
                    </div>
                    <button class="btn-large full-width" type="submit">KIRIM</button>
                </form>
            </div>
        </div>
    </div>
</div>