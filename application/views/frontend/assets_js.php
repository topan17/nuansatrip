<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo "{$_general_assets}plugins/jquery/1.11.1/jquery{$_sufiks_min_assets}.js"; ?>"></script>
<script src="<?php echo "{$_general_assets}plugins/jquery-ui/1.11.0/jquery-ui{$_sufiks_min_assets}.js"; ?>"></script>  

<!-- Juery Ui Addon CSS-->
<script src="<?php echo "{$_general_assets}plugins/jquery-ui/addon/jquery-ui-timepicker-addon{$_sufiks_min_assets}.js"; ?>"></script>  

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo "{$_general_assets}plugins/bootstrap/3.2.0/js/bootstrap{$_sufiks_min_assets}.js"; ?>"></script>

<script src="<?php echo "{$_general_assets_js}site_frontend.js"; ?>"></script>
<script src="<?php echo "{$_assets_js}recaptcha_ajax.js"; ?>"></script>

<!-- Load Js for current page -->
<?php if (isset($pages_js)): ?>
    <?php if (is_array($pages_js)): ?>
        <?php foreach ($pages_js as $js): ?>
            <script src="<?php echo $js; ?>"></script>
        <?php endforeach; ?>
    <?php else: ?>
        <script src="<?php echo $js; ?>"></script>
    <?php endif; ?>
<?php endif; ?>