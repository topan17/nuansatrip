<style>
    .chaser {
        background-color: #fff;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        z-index: 999;
        opacity: 0.95;
    }

    .chaser .logo {
        height: auto;
        margin-bottom: 0;
        margin-top: 14px;
        min-height: initial;
        padding: 0;
    }

    .chaser ul.menu > li > a {
        display: block;
        font-weight: normal;
        letter-spacing: 0.04em;
        height: 48px;
        line-height: 48px;
        padding: 0;
        text-transform: uppercase;
    }

    .chaser .logo img {
        height: 20px;
    }

    .chaser ul.dropdown-menu {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        margin-top: 0;
    }
</style>


<div class="chaser hidden-xs hidden-sm" style="display: none;">
    <div class="container">
        <h1 class="logo navbar-brand">
            <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                <img alt="Nuansa Trip" src="<?php echo "{$_assets}img/logo.png" ?>">
            </a>
        </h1>
        <ul class="menu" style="">
            <li><a href="<?php echo base_url(); ?>">HOME</a></li>
            <li><a href="<?php echo base_url('about-us'); ?>">ABOUT US</a></li>
            <li><a href="<?php echo base_url('contact-us'); ?>">CONTACT US</a></li>
            <?php echo $this->view->load('menu_product'); ?>
            <li><a href="<?php echo base_url('blog'); ?>">BLOG</a></li>
        </ul>
</div>
</div>