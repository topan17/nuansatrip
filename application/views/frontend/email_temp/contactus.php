<html>
    <head>
        <title>Contact us | nuansatrip.com</title>
    </head>
    <body>
        <div style="width:650px; margin: 50px auto; background-color: #fff; padding: 10px; font-family: 'Open Sans', Arial, sans-serif;">
            <div style="text-align: center;">
                <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                    <img style="width: 150px;" alt="Nuansa Trip" src="http://www.soaptheme.com/html/travelo/images/logo.png">
                </a>
                <hr style="border: 2px solid #01b7f2; margin: 7px 0;"/>
            </div>
            <div>
                <p>From: <span style="font-weight: bold;"><?php echo $contactus_name; ?></span>, Email: <span style="font-weight: bold;"><?php echo $contactus_email; ?></span></p>
                <p><?php echo $contactus_message; ?></p>
            </div>
        </div>
    </body>
</html>