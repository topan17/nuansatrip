<html>
    <head>
        <title>Aktivasi | nuansatrip.com</title>
    </head>
    <body>
        <div style="width:650px; margin: 50px auto; background-color: #fff; padding: 10px; font-family: 'Open Sans', Arial, sans-serif;">
            <div style="text-align: center;">
                <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                    <img style="width: 150px;" alt="Nuansa Trip" src="http://www.soaptheme.com/html/travelo/images/logo.png">
                </a>
                <hr style="border: 2px solid #01b7f2; margin: 7px 0;"/>
            </div>
            <div>
                <p>Dear <span style="font-weight: bold;"><?php echo $member_name; ?></span>,</p>
                <p style="font-size: 14px; font-weight: bold; color: #01b7f2;">Welcome to nuansatrip.com</p>
                <p>Terima kasih telah memililh nuansatrip.com untuk memulai pengalaman baru anda di dunia travel.</p>
                <p><a href="<?php echo base_url(); ?>/activasion?c=<?php echo $activasion_code; ?>&e=<?php echo $member_email; ?>" style="background-color: #01b7f2; padding: 5px 10px; color: #fff; border: medium none; text-decoration: none;">AKTIVASI</a></p>
                <p>Jika button diatas tidak mengarahkan ke halaman berikutnya, copy URL berikut ke browser anda: <a style="color: #01b7f2; text-decoration: none;" href="<?php echo base_url(); ?>/activasion?c=<?php echo $activasion_code; ?>&e=<?php echo $member_email; ?>">http://www.nuansatrip.com/activasion?c=<?php echo $activasion_code; ?>&e=<?php echo $member_email; ?></a></p>
                <br/><br/>
                <p style="font-weight: bold;">Happy Traveling</p>
                <br/>
                <p>Your Travel Buddies</p>
            </div>
        </div>
    </body>
</html>