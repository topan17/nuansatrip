<style>
    #recaptcha_div {
        background-color: #b71800;
        text-align: center;
        margin-bottom: 10px;
        padding: 5px;
        border-radius: 5px;
        color: #f5f5f5;
    }

    #recaptcha_div #recaptcha_image {
        margin-bottom: 7px;
    }

    #recaptcha_div #recaptcha_image span a {
        color: #f5f5f5 !important;
    }

    #recaptcha_div #recaptcha_image,
    #recaptcha_div #recaptcha_image img{
        width: 100% !important;
        height: auto !important;
        border-radius: 2px;
    }

    #recaptcha_div .solution {
        margin-bottom: 5px !important;
    }

    #recaptcha_div .solution label {
        font-size: 11px;
    }

    #recaptcha_div .solution input {
        color: #a9a9a9;
    }

    #recaptcha_div .options a {
        color: #f5f5f5;
        margin-right: 10px;
    }

    #recaptcha_div .options a:last-child {
        margin-right: 0;
    }
</style>


<div id="recaptcha_image"></div>
<div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>
<div class="form-group solution">
    <label class="recaptcha_only_if_image">Masukkan huruf atau angka diatas:</label>
    <label class="recaptcha_only_if_audio">Masukkan angka yang kamu dengar:</label>
    <input class="input-text full-width" type="text" id="recaptcha_response_field" name="recaptcha_response_field" placeholder="TYPE HERE">
</div>
<div class="options">
    <a href="javascript:Recaptcha.reload()"><i class="fa fa-refresh fa-2x"></i></a>
    <a class="recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')"><i class="fa fa-volume-up fa-2x"></i></a>
    <a class="recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')"><i class="fa fa-image fa-2x"></i></a>
    <a href="javascript:Recaptcha.showhelp()" id="icon-help"><i class="fa fa-question-circle fa-2x"></i></a>
</div>