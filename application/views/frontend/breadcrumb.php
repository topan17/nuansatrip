<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo generate_breadcrumb_title(end($this->uri->segment_array())); ?></h2>
        </div>
        <ul class="breadcrumbs pull-right hidden-sm hidden-xs">
            <li>
                <a href="<?php echo base_url(); ?>">HOME</a>
            </li>
            <?php 
                $segs = $this->uri->segment_array();
                $segment_url = "";
            ?>
            <?php foreach ($segs as $segment): ?>
            <?php if($segment ==  end($this->uri->segment_array())): ?>
                <li class="active">
                    <?php echo generate_breadcrumb_title($segment); ?>
                </li>
            <?php else: ?>
            <?php $segment_url .= $segment_url == "" ? $segment : "/".$segment; ?>
                <li>
                    <a href="<?php echo base_url($segment_url); ?>"><?php echo generate_breadcrumb_title($segment); ?></a>
                </li>
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>