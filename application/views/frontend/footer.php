<footer id="footer">
    <div class="bottom gray-area">
        <div class="container">
            <div class="logo col-sm-6">
                <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                    <img alt="Nuansa Trip" src="<?php echo "{$_assets}img/logo.png" ?>">
                </a>
            </div>
            <div class="copyright col-sm-6">
                <span>© 2014 Nuansa Trip</span>
            </div>
        </div>
    </div>
</footer>