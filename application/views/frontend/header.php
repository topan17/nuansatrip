<header id="header" class="navbar-static-top">
    <div class="topnav hidden-xs">
        <div class="container">
            <?php if ($this->session->userdata('is_login') === 'YES'): ?>
            <ul class="quick-menu pull-left">
                <li class="ribbon">
                    <a href="#">AKUN SAYA<i class="fa fa-sort"></i></a>
                    <ul class="menu mini">
                        <li>
                            <a title="Dashboard" href="#">PROFIL SAYA</a>
                        </li>
                        <li>
                            <a title="Dashboard" href="#">PESANAN SAYA</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <?php endif; ?>
            <ul class="quick-menu pull-right">
            <?php if ($this->session->userdata('is_login') === 'YES'): ?>
            <li><a href="<?php echo base_url('logout') . '?urlgoto=' . current_url(); ?>" class="goto-logout">KELUAR</a></li>
            <?php else: ?>
            <li><a href="#" class="goto-login">MASUK</a></li>
            <li><a href="#" class="goto-signup">DAFTAR</a></li>
            <?php endif; ?>
            </ul>
        </div>
    </div>
    <div class="main-header">
                <!--        <a class="mobile-menu-toggle" data-toggle="collapse" href="#mobile-menu-01">Mobile Menu Toggle</a>-->
        <div class="container">
            <h1 class="logo navbar-brand">
                <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                    <img alt="Nuansa Trip" src="<?php echo "{$_assets}img/logo.png" ?>">
                </a>
            </h1>
            <nav id="main-menu" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed btn-toggle-mobile" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav menu pull-right hidden-xs">
                        <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                        <li><a href="<?php echo base_url('about-us'); ?>">ABOUT US</a></li>
                        <li><a href="<?php echo base_url('contact-us'); ?>">CONTACT US</a></li>
                        <li class="dropdown">
                            <a href="<?php echo base_url('product'); ?>">PRODUCT <span class="caret"></span></a>
                            <?php echo $this->view->load('menu_product'); ?>
                        </li>
                        <li><a href="<?php echo base_url('blog'); ?>">BLOG</a></li>
                    </ul>

                    <ul class="nav navbar-nav menu-mobile hidden-lg hidden-md hidden-sm">
                        <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                        <li><a href="<?php echo base_url('about-us'); ?>">ABOUT US</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">PRODUCT <span class="caret"></span></a>
                            <?php echo $this->view->load('menu_product'); ?>
                        </li>
                        <li><a href="<?php echo base_url('blog'); ?>">BLOG</a></li>
                        <li><a href="#" class="goto-login">MASUK</a></li>
                        <li><a href="#" class="goto-signup">DAFTAR</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>