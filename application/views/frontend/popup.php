<style>
    .modal-custom .modal-content {
        border-radius: 0;
    }

    .modal-custom .ntrip-popup-login-box,
    .modal-custom .ntrip-popup-signup-box,
    .modal-custom .ntrip-popup-forgotpass-box,
    .modal-custom .ntrip-popup-alertactivasion-box,
    .modal-custom .ntrip-popup-activasion-box {
        background-color: #fff;
        border: 2px solid #01b7f2;
        padding: 20px;
    }

    .modal-custom .close-popup {
        position: absolute;
        top: -8px;
        right: -8px;
        cursor: pointer;
    }

    .modal-custom .form-group {
        margin-bottom: 10px;
    }

    .modal-custom .input-text {
        height: 34px;
        padding-left: 15px;
        padding-right: 15px;
        background: none repeat scroll 0 0 #f5f5f5;
        border: medium none;
        line-height: normal;
        color: #b9b9b9;
    }

    .modal-custom button.btn-login,
    .modal-custom button.btn-signup,
    .modal-custom button.btn-forgotpass,
    .modal-custom button.btn-activasion {
        font-size: 1.3333em;
        font-weight: 400;
        height: 36px;
        letter-spacing: 0.04em;
        line-height: 36px;
        margin-bottom: 10px;
        text-align: center;
        background-color: #a9a9a9;
    }

    .modal-custom button.btn-login:hover,
    .modal-custom button.btn-signup:hover,
    .modal-custom button.btn-forgotpass:hover,
    .modal-custom button.btn-activasion:hover {
        background-color: #98ce44;
    }

    .modal-custom p {
        font-size: 12px;
        color: #b9b9b9;
    }

    .modal-custom p a {
        font-size: 12px;
        color: #01b7f2;
    }

    .modal-custom .seperator {
        border-top: 1px solid #f5f5f5;
        line-height: 0;
        margin-bottom: 10px;
        margin-top: 10px;
        position: relative;
    }
</style>
<div class="modal modal-custom fade" id="popup_box_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="ntrip-popup-login-box">
                <div class="close-popup" data-dismiss="modal">
                    <span class="fa-stack fa-md">
                        <i style="color: #01b7f2;" class="fa fa-circle fa-stack-2x"></i>
                        <i style="color: #fff;" class="fa fa-close fa-stack-2x"></i>
                    </span>
                </div>
                <form id="form-popup-login">
                    <div class="form-group">
                        <input class="input-text full-width" name="login_email" type="text" placeholder="alamat email">
                    </div>
                    <div class="form-group">
                        <input class="input-text full-width" name="login_password" type="password" placeholder="kata sandi">
                    </div>
                    <div class="form-group">
                        <button class="btn-login full-width disabled" type="submit">MASUK</button>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Belum pernah daftar? <a class="goto-signup" href="#">Daftar</a></p>
                <p><a class="goto-forgotpass" href="#">Lupa kata sandi?</a></p>
                <p><a class="goto-request-activasion" href="#">Aktivasi?</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-custom fade" id="popup_box_signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="ntrip-popup-signup-box">
                <div class="close-popup" data-dismiss="modal">
                    <span class="fa-stack fa-md">
                        <i style="color: #01b7f2;" class="fa fa-circle fa-stack-2x"></i>
                        <i style="color: #fff;" class="fa fa-close fa-stack-2x"></i>
                    </span>
                </div>
                <form id="form-popup-register">
                    <div class="form-group">
                        <input class="input-text full-width" name="member_name" type="text" placeholder="nama lengkap">
                    </div>
                    <div class="form-group">
                        <input class="input-text full-width" name="member_email" type="text" placeholder="alamat email">
                    </div>
                    <div class="form-group">
                        <input class="input-text full-width" name="member_password" type="password" placeholder="kata sandi">
                    </div>
                    <div class="form-group">
                        <input class="input-text full-width" name="member_password_check" type="password" placeholder="ulangi kata sandi">
                    </div>
                    <div class="form-group">
                        <button class="btn-signup full-width" type="submit">DAFTAR</button>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Sudah pernah daftar? <a class="goto-login" href="#">Masuk</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-custom fade" id="popup_box_forgotpass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="ntrip-popup-forgotpass-box">
                <div class="close-popup" data-dismiss="modal">
                    <span class="fa-stack fa-md">
                        <i style="color: #01b7f2;" class="fa fa-circle fa-stack-2x"></i>
                        <i style="color: #fff;" class="fa fa-close fa-stack-2x"></i>
                    </span>
                </div>
                <form id="form-popup-forgotpass">
                    <div class="form-group">
                        <input class="input-text full-width" type="text" name="email_address" placeholder="alamat email">
                    </div>
                    <div class="form-group">
                        <button class="btn-forgotpass full-width" type="submit">KIRIM</button>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Kami akan mengirimkan password baru ke email anda, Sudah pernah daftar? <a class="goto-login" href="#">Masuk</a>, Belum pernah daftar? <a class="goto-signup" href="#">Daftar</a></p>
                <p>Anda butuh bantuan? Hubungi Kami melalui telepon <strong>0813-1-111-111</strong> atau email melalui <strong>nuansatrip@gmail.com</strong></p>
            </div>
        </div>
    </div>
</div>

<!--AKTIVASI EMAIL-->
<div class="modal modal-custom fade" id="popup_box_activasion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="ntrip-popup-activasion-box">
                <div class="close-popup" data-dismiss="modal">
                    <span class="fa-stack fa-md">
                        <i style="color: #01b7f2;" class="fa fa-circle fa-stack-2x"></i>
                        <i style="color: #fff;" class="fa fa-close fa-stack-2x"></i>
                    </span>
                </div>
                <form id="form-popup-activasion">
                    <div class="form-group">
                        <input class="input-text full-width" type="text" name="email_address" placeholder="alamat email">
                    </div>
                    <div class="form-group">
                        <button class="btn-activasion full-width" type="submit">KIRIM</button>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Kami akan mengirimkan link aktivasi ke email anda, Aktivasi akun Anda dengan klik AKTIVASI, Link aktivasi berlaku selama 1x24 jam.</p>
                <p>Anda butuh bantuan? Hubungi Kami melalui telepon <strong>0813-1-111-111</strong> atau email melalui <strong>nuansatrip@gmail.com</strong></p>
            </div>
        </div>
    </div>
</div>

<!--ALERT AKTIVASI-->
<div class="modal modal-custom fade" id="popup_box_alertactivasion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="ntrip-popup-alertactivasion-box">
                <div class="close-popup" data-dismiss="modal">
                    <span class="fa-stack fa-md">
                        <i style="color: #01b7f2;" class="fa fa-circle fa-stack-2x"></i>
                        <i style="color: #fff;" class="fa fa-close fa-stack-2x"></i>
                    </span>
                </div>
                <h5>Silahkan cek email anda</h5>
                <div class="seperator"></div>
                <p>Kami baru saja mengirimkan email konfirmasi ke alamat email <span class="member_email_activasion"></span>. Aktivasi akun Anda dengan klik AKTIVASI, Link aktivasi berlaku selama 1x24 jam.</p>
                <p>Anda butuh bantuan? Hubungi Kami melalui telepon <strong>0813-1-111-111</strong> atau email melalui <strong>nuansatrip@gmail.com</strong></p>
            </div>
        </div>
    </div>
</div>
