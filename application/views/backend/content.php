<div id="wrapper" class="sidebar">
    <div id="body-overlay" style="display: block;"></div>
    <div id="sidebar" class="col-lg-2">
        <div class="search-block clearfix">
            <span></span>
            <input type="text" placeholder="Are you looking for something?">
        </div>
        <ul class="drop-area">
            <li class="no-back">
                <a href="index.html">Dashboard</a>
            </li>
            <li>
                UI Elements
                <ul>
            </li>
            <li>
                Forms
                <ul>
            </li>
            <li class="">
                Pages
                <ul class="" style="display: none;">
                    <li>
                        <span class="icn icon-172"></span>
                        <a href="profile-and-timeline.html">Profile & Timeline</a>
                    </li>
                    <li>
                        <span class="icn icon-9"></span>
                        <a href="search-page.html">Search</a>
                    </li>
                    <li>
                        <span class="icn icon-143"></span>
                        <a href="invoice.html">Invoice</a>
                    </li>
                    <li>
                        <span class="icn icon-136"></span>
                        <a href="message-page.html">Messages</a>
                    </li>
                    <li>
                        <span class="icn icon-121"></span>
                        <a href="calendar.html">Calendar</a>
                    </li>
                    <li>
                        <span class="icn icon-50"></span>
                        <a href="gallery.html">Gallery</a>
                    </li>
                    <li>
                        <span class="icn icon-97"></span>
                        <a href="cowntdown.html">Page in progress</a>
                    </li>
                    <li>
                        <span class="icn icon-99"></span>
                        <a href="page-not-found.html">Page not found</a>
                    </li>
                    <li>
                        <span class="icn icon-133"></span>
                        <a href="typography.html">Typography</a>
                    </li>
                    <li>
                        <span class="icn icon-35"></span>
                        <a href="dropzone-page.html">Dropzone</a>
                    </li>
                    <li>
                        <span class="icn icon-129"></span>
                        <a href="notes.html">Notes</a>
                    </li>
                    <li>
                        <span class="icn icon-123"></span>
                        <a href="contact-page.html">Contact</a>
                    </li>
                </ul>
            </li>
            <li>
                Tables
                <ul>
                    <li>
                        <span class="icn icon-118"></span>
                        <a href="basic-tables.html">Basic tables</a>
                    </li>
                </ul>
            </li>
            <li>
                Login
                <ul>
                    <li>
                        <span class="icn icon-125"></span>
                        <a href="login-and-register.html">Login & Register</a>
                    </li>
                    <li>
                        <span class="icn icon-39"></span>
                        <a href="lock-screen.html">Lock screen</a>
                    </li>
                </ul>
            </li>
            <li>
                Charts
                <ul>
                    <li>
                        <span class="icn icon-156"></span>
                        <a href="charts.html">Basic Charts</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div id="wrap" class="col-lg-10">
        <div class="container dashboard">
            <div class="wrapper-dashboard">TEST</div>
        </div>
    </div>
</div>