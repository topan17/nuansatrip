<style>
    .navbar {
        background-color: #2D3E52;
        border: medium none;
        border-radius: 0;
        padding: 0;
        margin-bottom: 0;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
    }

    .navbar-header {
        padding: 10px 10px 10px 0;
    }

    .navbar-nav > li > a {
        color: #fff !important;
    }
    
    .nav .open > a, .nav .open > a:hover, .nav .open > a:focus {
        background-color: #2D3E52 !important;
    }
    
    .navbar-toggle {
        margin: 0 !important;
        border: none;
        border-radius: 0;
        padding: 2px;
        color: #fff;
    }
    
    .navbar-toggle:hover, .navbar-toggle:focus {
        background-color: inherit !important;
    }
    
    .dropdown-menu {
        background-color: #2D3E52;
        border: 1px solid #2D3E52;
        border-radius: 0;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
    }
    
    .dropdown-menu > li a {
        color: #fff;
        font-size: 12px;
    }
    
    .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
        background-color: inherit;
        color: #fff;
    }
    
    @media (max-width: 767px) {
        .navbar-header > a > img {
            padding-left: 15px;
        }
    }

    /*
    .navbar-brand {
        height: 59px;
        line-height: 30px;
        padding: 15px 25px;
        position: relative;
        width: 100%;
    }
    
    #sidebar-exp {
        display: inline-block;
        height: 40px;
        opacity: 1;
        position: absolute;
        right: 5px;
        top: 10px;
        transition: all 160ms ease 0s;
        padding: 7px;
    }
    
    .header-drop-right li a {
        color: rgb(255, 255, 255) !important;
        height: 59px;
        padding-top: 20px;
    }*/
</style>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                <i class="fa fa-bars fa-2x"></i>
            </button>
            <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                <img alt="Nuansa Trip" src="<?php echo "{$_assets}img/logo.png" ?>">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
<!--                <li><a href="#">Another Link</a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi, Taupan <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">My Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>