<?php if (!empty($breadcrumb)): ?>
    <!-- BEGIN PAGE TITLE & BREADCRUMB-->   
    <h3 class="page-title clearfix">
        <?php $menu_name = ucwords(preg_replace("/_/", "", $breadcrumb->menu_name)); ?>
        <?php echo $menu_name; ?>
        <small><?php echo $breadcrumb->menu_desc; ?></small>
        <?php if (isset($enable_crud) && $enable_crud == true && preg_match("/(add,|add$)/i", $privilege_action)): ?>
            <a href="<?php echo "{$class_url}add"; ?>" class="pull-right btn btn-primary btn-action"><span class="glyphicon glyphicon-plus"></span>&nbsp;New</a>
        <?php endif; ?>
    </h3>
    <ul class="page-breadcrumb breadcrumb">
        <?php
        $segment_arr = $this->uri->segment_array();
        $count_segment = count($segment_arr);
        ?>
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo $template_url; ?>">Home</a> <i class="fa fa-angle-right"></i> 
        </li>
        <?php echo $breadcrumb_parent; ?>
        <?php foreach ($segment_arr as $key => $val): ?>
            <?php if ($key > 1): ?>
                <li>
                    <?php if ($key == 2): ?>
                        <a href="<?php echo "{$template_url}{$breadcrumb->menu_segment}"; ?>"><?php echo $menu_name; ?></a>
                    <?php else: ?>
                        <a href="<?php echo "{$template_url}{$breadcrumb->menu_segment}/{$val}"; ?>"><?php echo ucwords(preg_replace("/_/", "", $val)); ?></a>
                    <?php endif; ?>
                    <?php echo $count_segment > $key ? ' <i class="fa fa-angle-right"></i> ' : '' ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
    <!-- END PAGE TITLE & BREADCRUMB--> 
<?php endif; ?>