<style>
    #privilege-result {
        background-color: #f9f9f9;
        margin-top: 50px;
        padding: 10px;
    }

    #privilege-result tr td label {
        margin-bottom: 0 !important;
    }

    #privilege-result tr td label.checkbox-inline {
        padding: 0;
    }

    #privilege-result input[type="checkbox"] {
        position: relative;
        margin: 0 5px 0 0;
    }

    .table-responsive {
        border: none;
    }
</style>

<div class="box-white">
    <div class="clearfix">
        <form id="form-privilege" class="form-horizontal" role="form" class="clearfix">
            <div class="form-group">
                <label for="user_group_id" class="col-sm-2 control-label">USER GROUP</label>
                <div class="col-sm-10 controls">
                    <select name="user_group_id" id="user_group_id">
                        <option value="0">PLEASE SELECT</option>
                        <?php foreach ($user_groups as $user_group): ?>
                            <option value="<?php echo $user_group->group_id; ?>"><?php echo $user_group->group_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table" id="privilege-result"></table>
            </div>
            <div class="button-area">
                <button class="btn btn-success" type="submit">SAVE</button>
            </div>
        </form>
    </div>
</div>

