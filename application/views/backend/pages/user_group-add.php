<div class="box-white">
    <div class="clearfix">
        <form id="form-user-group-add" class="form-horizontal" role="form" class="clearfix form">
            <div class="form-group">
                <label for="group_name" class="col-sm-2 control-label">GROUP NAME</label>
                <div class="col-sm-10 controls">
                    <input class="input-text" type="text" placeholde="Group Name" name="group_name">
                </div>
            </div>
            <div class="form-group">
                <label for="group_active" class="col-sm-2 control-label">ACTIVE</label>
                <div class="col-sm-10 controls">
                    <label class="radio-inline">
                        <input type="radio" name="group_active" id="group_active" value="YES"> YES
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="group_active" id="group_active" value="NO"> NO
                    </label>
                </div>
            </div>
            <div class="button-area">
                <a class="btn btn-warning" href="<?php echo $this->class_url; ?>">BACK</a>
                <button class="btn btn-success" type="submit">SAVE</button>
            </div>
        </form>
    </div>
</div>

