<?php if (preg_match('/edit/i', $data_action)) : ?>
    <a href="<?php echo current_url . 'edit'; ?>"><i class="fa fa-edit icon"></i></a>
<?php endif; ?>

<?php if (preg_match('/delete/i', $data_action)) : ?>
    <a href="#"><i class="fa fa-trash icon"></i></a>
<?php endif; ?>
