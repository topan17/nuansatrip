<div class="box-white">
    <div class="clearfix">
        <form id="form-user-group-edit" class="form-horizontal" role="form" class="clearfix form">
            <input type="hidden" name="group_id" value="<?php echo $data_form->row()->group_id; ?>" />
            <div class="form-group">
                <label for="group_name" class="col-sm-2 control-label">GROUP NAME</label>
                <div class="col-sm-10 controls">
                    <input class="input-text" type="text" placeholde="Group Name" name="group_name" value="<?php echo $data_form->row()->group_name; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="group_active" class="col-sm-2 control-label">ACTIVE</label>
                <div class="col-sm-10 controls">
                    <?php
                        $checked_yes = ($data_form->row()->group_active == 'YES') ? "checked" : "";
                        $checked_no = ($data_form->row()->group_active == 'NO') ? "checked" : "";
                    ?>
                    <label class="radio-inline">
                        <input type="radio" name="group_active" id="group_active" value="YES" <?php echo $checked_yes; ?> > YES
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="group_active" id="group_active" value="NO" <?php echo $checked_no; ?> > NO
                    </label>
                </div>
            </div>
            <div class="button-area">
                <a class="btn btn-warning" href="<?php echo $this->class_url; ?>">BACK</a>
                <button class="btn btn-success" type="submit">SAVE</button>
            </div>
        </form>
    </div>
</div>

