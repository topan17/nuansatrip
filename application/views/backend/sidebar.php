<style>
    .sidebar-nav {
        padding: 10px;
        font-size: 14px;
        font-weight: bold;
    }

    .list-menu a.list-group-item {
        border-radius: 0 !important;
        padding: 5px;
        margin-bottom: 5px;
    }

    .list-menu a.list-group-item i.fa-caret-down {
        /*        float: right;
                margin-top: 5px;*/
        position: absolute;
        display: inline-block;
        right: 5px;
        top: 10px;
    }

    .list-menu a.list-group-item i.icon {
        margin-right: 5px;
    }

    .list-menu > .list-group-item-parent,  
    .list-menu > .list-group-item-parent:hover,
    .list-menu > .list-group-item-parent:focus {
        background-color: #01B7F2;
        color: #fff;
        border: 1px solid #01B7F2;
    }

    .list-menu .list-group-submenu a {
        border: none;
    }

    .list-menu > .list-group-submenu a.active,
    .list-menu > .list-group-submenu a.active:hover,
    .list-menu > .list-group-submenu a.active:focus {
        background-color: #F5F5F5;
        color: #555;
    }
</style>

<?php
//echo '<pre>';
//print_r($this->session->userdata('sidebar_menu'));
//echo '</pre>';
?>

<div class="box-white sidebar-nav">
    <h5 class="box-title">MENU</h5>
    <div class="list-menu">
        <?php echo $sidebar_menu; ?>
    </div>
</div>