<style>
    #footer .bottom {
        height: auto;
        border-top: 2px solid #888;
        color: #fff;
    }
    .first-area {
        background: none repeat scroll 0 0 #2D3E52;
    }

    #footer .container{
        padding: 0;
    }

    #footer .bottom .logo {
        margin: 18px 0;
        text-align: left;
        padding: 0;
    }

    #footer .bottom .logo img {
        height: 20px;
    }

    #footer .bottom .copyright {
        font-size: 1.0833em;
        margin: 18px 0;
        text-align: right;
        padding: 0;
    }

    @media (max-width: 767px) {
        #footer .container{
            padding-left: 15px;
            padding-right: 15px;
            width: auto;
        }

        #footer .bottom .logo,
        #footer .bottom .copyright {
            text-align: center;
        }

        #footer .bottom .copyright {
            margin: 7px 0;
            font-size: 11px;
        }
    }
</style>

<footer id="footer">
    <div class="bottom first-area">
        <div class="container-fluid">
            <div class="logo col-sm-6">
                <a title="Nuansa Trip" href="<?php echo base_url(); ?>">
                    <img alt="Nuansa Trip" src="<?php echo "{$_assets}img/logo.png" ?>">
                </a>
            </div>
            <div class="copyright col-sm-6">
                <span>© 2014 Nuansa Trip</span>
            </div>
        </div>
    </div>
</footer>