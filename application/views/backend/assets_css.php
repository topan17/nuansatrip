<!-- Jquery Ui CSS -->
<link rel="stylesheet" href="<?php echo "{$_general_assets}plugins/jquery-ui/1.11.0/jquery-ui{$_sufiks_min_assets}.css"; ?>">

<!-- Juery Ui Addon CSS-->
<link rel="stylesheet" href="<?php echo "{$_general_assets}plugins/jquery-ui/addon/jquery-ui-timepicker-addon{$_sufiks_min_assets}.css"; ?>">

<!-- Bootstrap -->
<link href="<?php echo "{$_general_assets}plugins/bootstrap/3.2.0/css/bootstrap{$_sufiks_min_assets}.css"; ?>" rel="stylesheet">
<!-- End Bootstrap -->

<!-- Site CSS -->
<link rel="stylesheet" href="<?php echo "{$_assets}css/site{$_sufiks_min_assets}.css"; ?>">

<!-- Load Css for current page -->
<?php if (isset($pages_css)): ?>
    <?php if (is_array($pages_css)): ?>
        <?php foreach ($pages_css as $css): ?>
            <link rel="stylesheet" href="<?php echo $css; ?>">
        <?php endforeach; ?>
    <?php else: ?>
        <link rel="stylesheet" href="<?php echo $pages_css; ?>">
    <?php endif; ?>
<?php endif; ?>