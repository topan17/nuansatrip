<style>
    .modal-custom .modal-content {
        border-radius: 0;
    }

    .modal-custom .box_del_confirmation {
        background-color: #fff;
        border: 2px solid #01b7f2;
        padding: 20px;
    }

    .modal-custom .close-popup {
        position: absolute;
        top: -8px;
        right: -8px;
        cursor: pointer;
    }

    .modal-custom .form-group {
        margin-bottom: 10px;
    }

    .modal-custom .input-text {
        height: 34px;
        padding-left: 15px;
        padding-right: 15px;
        background: none repeat scroll 0 0 #f5f5f5;
        border: medium none;
        line-height: normal;
        color: #b9b9b9;
    }

    .modal-custom button, .modal-custom a.button {
        border: medium none;
        color: rgb(255, 255, 255);
        cursor: pointer;
        padding: 0 15px;
        white-space: nowrap;
    }

    .modal-custom button.btn-del-conf-yes,
    .modal-custom button.btn-del-conf-no {
        font-size: 1.3333em;
        font-weight: 400;
        height: 36px;
        letter-spacing: 0.04em;
        line-height: 36px;
        margin-bottom: 10px;
        text-align: center;
        background-color: #a9a9a9;
    }

    .modal-custom button.btn-del-conf-yes:hover,
    .modal-custom button.btn-del-conf-no:hover {
        background-color: #98ce44;
    }

    .modal-custom p {
        font-size: 12px;
        color: #b9b9b9;
    }

    .modal-custom p a {
        font-size: 12px;
        color: #01b7f2;
    }

    .modal-custom .seperator {
        border-top: 1px solid #f5f5f5;
        line-height: 0;
        margin-bottom: 10px;
        margin-top: 10px;
        position: relative;
    }

    .modal-custom .full-width {
        width: 100% !important;
    }
</style>
<div class="modal modal-custom fade" id="popup_box_del_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="box_del_confirmation clearfix">
                <div class="close-popup" data-dismiss="modal">
                    <span class="fa-stack fa-md">
                        <i style="color: #01b7f2;" class="fa fa-circle fa-stack-2x"></i>
                        <i style="color: #fff;" class="fa fa-close fa-stack-2x"></i>
                    </span>
                </div>
                <p class="content"></p>
                <div class="seperator"></div>
                <div class="col-md-6">
                    <button class="btn-del-conf-yes full-width">YES</button>
                </div>
                <div class="col-md-6">
                    <button class="btn-del-conf-no full-width">NO</button>
                </div>
            </div>
        </div>
    </div>
</div>
