<!DOCTYPE html>
<html lang="en">
    <?php if ($class_name === 'login'): ?>
        <head>
            <title>Nuansatrip | Backend Login</title>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?php $this->view->load("assets_css"); ?>
            <?php $this->view->load("font"); ?>
        </head>
        <body>
            <!-- BEGIN CONTAINER -->
            <div class="logo">
                <a href="index.html">
                    <img alt="" src="http://nuansatrip.net/assets/frontend/img/logo.png">
                </a>
            </div>
            <div class="content">
                <form class="login-form" method="post" action="index.html" novalidate="novalidate">
                    <h3 class="form-title">Sign In</h3>
                    <div class="form-group">
                        <input class="form-control" type="text" name="username" placeholder="Username" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="password" placeholder="Password" autocomplete="off">
                    </div>
                    <div class="form-actions">
                        <button class="btn btn-success uppercase col-xs-12" type="submit">MASUK</button>
                        <a href="#" id="forget-password" class="forget-password">Lupa kata sandi?</a>
                    </div>
                </form>
            </div>
            <div class="copyright">2014 © Nuansatrip</div>
        </body>
        <?php $this->view->load("assets_js"); ?>
    <?php else: ?>
        <head>
            <meta charset="utf-8" />
            <title>Nuansatrip | Backend</title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <?php //$this->view->load("font"); ?>
            <?php $this->view->load("assets_css"); ?>
            <?php $this->view->load("font"); ?>
            <?php $this->view->load("global_js_var.php"); ?>
        </head>
        <body>
            <?php $this->view->load("header"); ?>
            <div id="content">
                <div class="container-fluid">
                    <div class="row">
                    <div class="sidebar col-md-3">
                        <?php echo $this->view->load('sidebar'); ?>
                    </div>
                    <div id="main" class="col-md-9" style="min-height: 750px;">
                        <?php $this->view->load($_pages); ?>
                    </div>
                    </div>
                </div>
            </div>
<!--            <div class="col-lg-3 clearfix" style="border: 1px solid green;">
                <?php $this->view->load("sidebar"); ?>
            </div>
            <div class="col-lg-9 clearfix" style="border: 1px solid red;">
                TEST
            </div>-->
            <?php //$this->view->load($_pages); ?>
            <?php $this->view->load("footer"); ?>
            <?php $this->view->load("popup"); ?>
            <?php $this->view->load("assets_js"); ?>

            <!--tinymce editor-->
            <?php if (isset($enable_crud) && $enable_crud == TRUE): ?>
                <?php $this->view->load("pages/tinymce/media_dialog"); ?>
            <?php endif; ?>
        </body>
    <?php endif; ?>
</html>